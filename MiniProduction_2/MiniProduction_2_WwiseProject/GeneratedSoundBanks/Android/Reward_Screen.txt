Event	ID	Name			Wwise Object Path	Notes
	15513699	stop_zipper_loop			\Default Work Unit\Reward Screen\stop_zipper_loop	
	312342947	play_bloody_reward			\Default Work Unit\Reward Screen\play_bloody_reward	
	452547817	stop_all			\Default Work Unit\Musix\stop_all	
	3149298709	play_zipper_loop			\Default Work Unit\Reward Screen\play_zipper_loop	
	3447121007	play_crowd_lose			\Default Work Unit\Reward Screen\play_crowd_lose	
	4259944086	play_crowd_win			\Default Work Unit\Reward Screen\play_crowd_win	

Effect plug-ins	ID	Name	Type				Notes
	1159390747	Room_Medium_Tiled	Wwise RoomVerb			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	224708240	crowd_win	Y:\Documents\GitHub\MiniProduction_2\MiniProduction_2\MiniProduction_2_WwiseProject\.cache\Android\SFX\crowd_win_4F00D835.wem		\Actor-Mixer Hierarchy\Default Work Unit\Reward Screen\crowd_win		507948
	406566520	Bloody reward	Y:\Documents\GitHub\MiniProduction_2\MiniProduction_2\MiniProduction_2_WwiseProject\.cache\Android\SFX\Bloody reward_4F00D835.wem		\Actor-Mixer Hierarchy\Default Work Unit\Reward Screen\Bloody reward		50011
	743875766	Zipper_loop	Y:\Documents\GitHub\MiniProduction_2\MiniProduction_2\MiniProduction_2_WwiseProject\.cache\Android\SFX\Zipper_loop_6C6B4BA3.wem		\Actor-Mixer Hierarchy\Default Work Unit\Reward Screen\Zipper_loop		69614
	1043234251	crowd_loose	Y:\Documents\GitHub\MiniProduction_2\MiniProduction_2\MiniProduction_2_WwiseProject\.cache\Android\SFX\crowd_loose_4F00D835.wem		\Actor-Mixer Hierarchy\Default Work Unit\Reward Screen\crowd_loose		272216

