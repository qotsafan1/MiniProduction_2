﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : Manager<GameLogic> {

	[SerializeField]
	GameObject draggableObject;
	public bool isPlayerAttacking;
	public float playerAttackTimeAmount;
	public float timeBetweenAttacks;
	public float currentTime;
	List<int> turns;
	int currentRound;
	int playerToken;
	

	public bool isEnemyHit;
	public bool playerReachedApex;
	void Awake()
	{
		isPlayerAttacking = true;
		playerReachedApex = false;
		isEnemyHit = false;
		currentRound = 0;
		CalculateEuclidean(StatsController.Instance().agility,StatsController.Instance().agilityEnemy);
		for (int i = 0; i < turns.Count; i++)
		{
			//Debug.Log(turns[i]);
		}
		PrintRoundOrder();
	}
	public void RecalculatedTurnOrder()
	{	
		int nextRound = 0;
		if (turns.Count -1 > currentRound)
		{
			nextRound = currentRound + 1;
		}
		int nextTurn = turns[nextRound];
		Debug.Log("Current Round: " + currentRound + " Next turn: " + nextTurn);

		CalculateEuclidean(StatsController.Instance().agility,StatsController.Instance().agilityEnemy);
		
		//Need to find the new current round. starting from the old currentround until you get to a round that have the same turn as your current next turn
		int tempCurrentRound = currentRound;
		for (int i = 0; i < turns.Count; i++)
		{
			//Debug.Log("TempCurrentRound: " + tempCurrentRound + " with I: " + i + " and turnsCount: " +  turns.Count);
			tempCurrentRound++;
			if (tempCurrentRound >= turns.Count)
			{
				tempCurrentRound = 0;
			}
			if (turns[tempCurrentRound] == nextTurn)
			{
				currentRound = tempCurrentRound;
				break;
			}
		}
		PrintRoundOrder();

	}
	

	void NextRound()
	{
		currentRound++;
		if (turns.Count == currentRound)
		{
			currentRound = 0;
		}
		if (turns[currentRound] == 0)
		{
			PrepareForPlayerTurn();
		} else {
			PrepareForEnemyTurn();
		}
		currentTime = 0.0f;
		ArrowScript.Instance().DeactivateArrow();

	}

	public void PlayerAttacked()
	{
		if (isPlayerAttacking)
		{
			NextRound();
			draggableObject.SetActive(false);
			playerReachedApex = false;

		} 
	}
	void PrepareForPlayerTurn()
	{
		draggableObject.SetActive(true);
		playerReachedApex = false;
		isPlayerAttacking = true;
		isEnemyHit = false;
		Debug.Log("Player Turn");

	}
	void PrepareForEnemyTurn()
	{
		draggableObject.SetActive(false);
		playerReachedApex = false;
		isPlayerAttacking = false;
		Debug.Log("Enemy Turn");

	}
	
	void CalculateEuclidean(int playerAgility, int enemyAgility)
	{
		int pulses = Mathf.Max(playerAgility,enemyAgility);
		int steps = playerAgility+enemyAgility;
		if (pulses == playerAgility)
		{
			playerToken = 0;
		} else 
		{
			playerToken = 1;
		}

    	int bucket = 0; //out variable to add pulses together for each step
		turns = new List<int>();

    	//fill array with rhythm
		for( int i=0 ; i < steps ; i++)
		{ 
			bucket += pulses; 
			if(bucket >= steps) 
			{
				bucket -= steps;
				if (playerToken == 0)
				{
					turns.Add(0); 
				} else 
				{
					turns.Add(1);
				}

			} else 
			{
				if (playerToken == 0)
				{
					turns.Add(1); //'1' indicates a pulse on this beat
				} else 
				{
					turns.Add(0);
				}
			}
		}

	}
	public int[] GetNextXTurns(int numberOfTurns)
	{
		int tempCurrentRound = currentRound;
		if (tempCurrentRound == -1)
		{
			tempCurrentRound = 0;
		}
		int[] tempTurns = new int[numberOfTurns];

		for (int i = 0; i < numberOfTurns; i++)
		{
			if (tempCurrentRound >= turns.Count)
			{
				tempCurrentRound = 0;
			}
			tempTurns[i] = turns[tempCurrentRound];
			tempCurrentRound++;
		}
		return tempTurns;
	}

    void Update()
    {
        currentTime += Time.deltaTime;

		if (isPlayerAttacking)
		{
			if (currentTime > playerAttackTimeAmount)
			{
				//Time is up for the player Attack
				NextRound();
			}
		} else {
			if (currentTime > timeBetweenAttacks)
			{
				//Player can now attack
				NextRound();
				HealthController.Instance().PlayerTakeDamage();
			}
		}
    }
	void PrintRoundOrder()
	{
		string toPrint = "Current round: " + currentRound + " the turns count: " + turns.Count + ": ";
		for (int i = 0; i < turns.Count; i++)
		{
			toPrint += turns[i].ToString() + ", ";
		}
		Debug.Log(toPrint);
	}
}
