﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowScript : Manager<ArrowScript> {
	[SerializeField]
	Image arrow;
	[SerializeField]
	Transform draggableObject;
	bool isShowing;
	Vector2 startPosition;
	Quaternion startRotation;
	Vector2 originalPositionDraggableObject;
	float distance;
	// Use this for initialization
	void Start () {
		startPosition = arrow.transform.position;
		startRotation = arrow.transform.rotation;
		originalPositionDraggableObject = draggableObject.position;
		distance = Vector3.Distance(arrow.transform.position, draggableObject.position);
	}
	
	// Update is called once per frame
	void Update () {
		if (isShowing)
		{
			MoveArrow();
		}
	}

	void MoveArrow()
	{
		//get angle, normalise, reverse angle multiply by distance. move arrow. Rotate?
		if (Input.touchCount != 1) {return;}
		Vector2 oriAngle = originalPositionDraggableObject - Input.touches[0].position;
		Vector2 newPosition = (oriAngle.normalized * distance) + originalPositionDraggableObject;
		
		if (newPosition.x > originalPositionDraggableObject.x)
		{
			newPosition.x = originalPositionDraggableObject.x;
		}
		
		arrow.transform.position = newPosition;
		
		
		
		float newAngle =  Vector2.Angle(oriAngle.normalized,Vector2.up);
		if (newAngle < 0)
		{
			newAngle = 0;
		} else if (newAngle> 180)
		{
			newAngle = 180;
		}


    	arrow.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,newAngle);
	}
	void ResetArrow()
	{
		arrow.transform.position = startPosition;
		arrow.transform.rotation = startRotation;
	}
	public void ActivateArrow()
	{
		isShowing = true;
		arrow.gameObject.SetActive(true);
	}

	public void DeactivateArrow()
	{
		isShowing = false;
		arrow.gameObject.SetActive(false);
		ResetArrow();
	}

}
