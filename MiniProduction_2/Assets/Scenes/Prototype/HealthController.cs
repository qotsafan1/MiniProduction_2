﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : Manager<HealthController> {
	public enum HitZone {Head,Stomach,Groin};
	[SerializeField]
	public Image playerHealth;
	[SerializeField]
	public Image enemyHealth;
	[SerializeField]
	public Image playerDamage;
	[SerializeField]
	public Image enemyDamage;
	[SerializeField]
	Image timer;

	float enemyFloat;
	float playerFloat;
	[SerializeField]
	Transform parent;

	void Start()
	{
		playerFloat = 100.0f;
		enemyFloat = 100.0f;
	}

	public void EnemyTakeDamage(HitZone zone)
	{
		if (GameLogic.Instance().isEnemyHit /*|| GameLogic.Instance()().playerReachedApex*/) {return;}
		GameLogic.Instance().isEnemyHit = true;
		switch (zone)
		{
			case HitZone.Head:
			StatsController.Instance().currentEnemyHealth -= CalculatePlayerDamage(1.5f);
			break;
			case HitZone.Stomach:
			StatsController.Instance().currentEnemyHealth -=CalculatePlayerDamage(1.0f);
			break;
			case HitZone.Groin:
			StatsController.Instance().currentEnemyHealth -= CalculatePlayerDamage(0.75f);
			StatsController.Instance().agilityEnemy--;
			GameLogic.Instance().RecalculatedTurnOrder();
			break;
		}
		UpdateUI();
		
	}
	float CalculateEnemyDamage()
	{
		float damage = (StatsController.Instance().strenghtEnemy + Random.Range(-1,1));
		//float reduction = ((StatsController.Instance().defense)*0.06f)/(1+0.06f*(StatsController.Instance().defense));
		float reduction = StatsController.Instance().defense / 100.0f;
		if (damage < 1)
		{
			damage = 1;
		}
		return (damage -(damage * reduction));
		
	}
	float CalculatePlayerDamage(float multiplier)
	{
		float damage = (StatsController.Instance().strenght + Random.Range(-1,1));
		//float reduction = ((StatsController.Instance().defenseEnemy)*0.06f)/(1+0.06f*(StatsController.Instance().defenseEnemy));
		float reduction = StatsController.Instance().defenseEnemy / 100.0f;
		if (damage < 1)
		{
			damage = 1;
		}
		return (damage -(damage * reduction)) * multiplier;
	}

	void CheckForHealthLowerThanZero()
	{
		bool change = false;
		if (StatsController.Instance().currentHealth < 0)
		{
			change = true;
			Debug.Log("Player Lost");
		} else if (StatsController.Instance().currentEnemyHealth < 0)
		{
			change = true;
			Debug.Log("Enemy Lose");
		}

		if (change)
		{
			StatsController.Instance().currentHealth = StatsController.Instance().health ;
			StatsController.Instance().currentEnemyHealth  = StatsController.Instance().healthEnemy ;
		}
	}

	public void PlayerTakeDamage(float amount)
	{
		//playerFloat -= amount;
		StatsController.Instance().currentHealth -= CalculateEnemyDamage();
		//=((armor)*0.06)/(1+0.06*(armor))
		//Debug.Log(((80)*0.06)/(1+0.06*(80)));
		UpdateUI();
	}
	public void PlayerTakeDamage()
	{
		StatsController.Instance().currentHealth -= CalculateEnemyDamage();
		
		UpdateUI();
	}
	
	void UpdateUI()
	{
		CheckForHealthLowerThanZero();
		float fillamount = StatsController.Instance().currentHealth / StatsController.Instance().health;
		float damageAmount = playerHealth.fillAmount - fillamount;
		if (playerHealth.fillAmount != fillamount)
		{	
			playerDamage.gameObject.SetActive(true);
			//player damager occured
			//find the previous fillamount. thats the x position
			//set size according to the damage fillamount.
			Vector2 damageSize = playerHealth.GetComponent<RectTransform>().sizeDelta;
			damageSize.x *= damageAmount;
			//Tag den fulde størrelse, plus fillamount og halvdelen af damage amout. Set det som ny position.
			Vector2 damagePosition = playerHealth.GetComponent<RectTransform>().position;
			damagePosition.x -= ((playerHealth.GetComponent<RectTransform>().sizeDelta.x) /2);
			damagePosition.x += (playerHealth.GetComponent<RectTransform>().sizeDelta.x *fillamount) + ((playerHealth.GetComponent<RectTransform>().sizeDelta.x *damageAmount)/2);
			Debug.Log("Previous position: " + playerDamage.GetComponent<RectTransform>().position + " and size: " + playerDamage.GetComponent<RectTransform>().sizeDelta );
			playerDamage.GetComponent<RectTransform>().position = damagePosition; 
			playerDamage.GetComponent<RectTransform>().sizeDelta = damageSize;
			Debug.Log("New position: " + playerDamage.GetComponent<RectTransform>().position + " and size: " + playerDamage.GetComponent<RectTransform>().sizeDelta );
			StartRemovingDamageIndicator("Player");
		}
		fillamount = StatsController.Instance().currentEnemyHealth / StatsController.Instance().healthEnemy;
		damageAmount = enemyHealth.fillAmount - fillamount;
		if (enemyHealth.fillAmount != fillamount)
		{	
			enemyDamage.gameObject.SetActive(true);
			//player damager occured
			//find the previous fillamount. thats the x position
			//set size according to the damage fillamount.
			Vector2 damageSize = enemyHealth.GetComponent<RectTransform>().sizeDelta;
			damageSize.x *= damageAmount;
			//Tag den fulde størrelse, plus fillamount og halvdelen af damage amout. Set det som ny position.
			Vector2 damagePosition = enemyHealth.GetComponent<RectTransform>().position;
			damagePosition.x -= ((enemyHealth.GetComponent<RectTransform>().sizeDelta.x) /2);
			damagePosition.x += (enemyHealth.GetComponent<RectTransform>().sizeDelta.x *fillamount) + ((enemyHealth.GetComponent<RectTransform>().sizeDelta.x *damageAmount)/2);
			Debug.Log("Previous position: " + enemyDamage.GetComponent<RectTransform>().position + " and size: " + enemyDamage.GetComponent<RectTransform>().sizeDelta );
			enemyDamage.GetComponent<RectTransform>().position = damagePosition; 
			enemyDamage.GetComponent<RectTransform>().sizeDelta = damageSize;
			Debug.Log("New position: " + enemyDamage.GetComponent<RectTransform>().position + " and size: " + enemyDamage.GetComponent<RectTransform>().sizeDelta );
			StartRemovingDamageIndicator("Enemy");
		}


		playerHealth.fillAmount = StatsController.Instance().currentHealth / StatsController.Instance().health;;
		enemyHealth.fillAmount = StatsController.Instance().currentEnemyHealth / StatsController.Instance().healthEnemy;

		//Debug.Log("Player Health: " + playerFloat + " EnemyHealth: " + enemyFloat);
	}
	void StartRemovingDamageIndicator(string who)
	{
		if (who == "Player")
		{
			InvokeRepeating("DeminishPlayerDamageIndicator",1.0f,Time.fixedDeltaTime);
		} else {
			InvokeRepeating("DeminishEnemyDamageIndicator",1.0f,Time.fixedDeltaTime);
		}
	}

	void DeminishPlayerDamageIndicator()
	{
		playerDamage.fillAmount = playerDamage.fillAmount - Time.fixedDeltaTime;
		if (playerDamage.fillAmount <= 0.0f)
		{
			playerDamage.gameObject.SetActive(false);
			playerDamage.fillAmount = 1.0f;
			CancelInvoke("DeminishPlayerDamageIndicator");
		}
	}
	void DeminishEnemyDamageIndicator()
	{
		enemyDamage.fillAmount = enemyDamage.fillAmount - Time.fixedDeltaTime;
		if (enemyDamage.fillAmount <= 0.0f)
		{
			enemyDamage.gameObject.SetActive(false);
			enemyDamage.fillAmount = 1.0f;
			CancelInvoke("DeminishEnemyDamageIndicator");
		}

	}

	void Update()
	{
		Image[] turnOrderObjects = parent.GetComponentsInChildren<Image>();

		int[] comingTurns =  GameLogic.Instance().GetNextXTurns(turnOrderObjects.Length);

		for (int i = 0; i < turnOrderObjects.Length; i++)
		{
			if (comingTurns[i] == 0)
			{

				turnOrderObjects[i].color = Color.yellow;
			} else {
				turnOrderObjects[i].color = Color.blue;

			}
		}

		UpdateTimer();
		
	}
	void UpdateTimer()
	{
		float turnTime;
		
		if (parent.GetComponentsInChildren<Image>()[0].color == Color.yellow)
		{
			turnTime = GameLogic.Instance().playerAttackTimeAmount;
		} else {
			turnTime = GameLogic.Instance().timeBetweenAttacks;
		}
		float fillamount = GameLogic.Instance().currentTime / turnTime;
		timer.fillAmount = fillamount;
	}
}
