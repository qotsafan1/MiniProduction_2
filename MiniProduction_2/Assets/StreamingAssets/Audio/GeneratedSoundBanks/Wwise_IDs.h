/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_ANNOUNCER_BAD_HIT = 1166817897U;
        static const AkUniqueID PLAY_ANNOUNCER_FIGHT = 1434098536U;
        static const AkUniqueID PLAY_ANNOUNCER_GOOD_HIT = 1284480111U;
        static const AkUniqueID PLAY_ANNOUNCER_MED_HIT = 992833398U;
        static const AkUniqueID PLAY_ANNOUNCER_WELCOME = 3535858286U;
        static const AkUniqueID PLAY_BLOOD_LUST = 3311070109U;
        static const AkUniqueID PLAY_BLOODY_REWARD = 312342947U;
        static const AkUniqueID PLAY_CLICK = 311910498U;
        static const AkUniqueID PLAY_CLICK_2 = 3821598057U;
        static const AkUniqueID PLAY_CLICK_BACK = 400143964U;
        static const AkUniqueID PLAY_CLICK_WRONG = 1286214044U;
        static const AkUniqueID PLAY_CROWD = 639442169U;
        static const AkUniqueID PLAY_CROWD_LOSE = 3447121007U;
        static const AkUniqueID PLAY_CROWD_WIN = 4259944086U;
        static const AkUniqueID PLAY_DISPLAY_AMBIENCE = 807277571U;
        static const AkUniqueID PLAY_FEMALE_GRUNT_2 = 3064969470U;
        static const AkUniqueID PLAY_GRINDER = 3938643945U;
        static const AkUniqueID PLAY_MALE_GRUNT_1 = 2553358554U;
        static const AkUniqueID PLAY_MENU_AMBIENCE = 3738477218U;
        static const AkUniqueID PLAY_MENU_MUSIC = 2228153899U;
        static const AkUniqueID PLAY_PUNCH_HIT = 3079399424U;
        static const AkUniqueID PLAY_ROTUNDA = 137903989U;
        static const AkUniqueID PLAY_UI_CLOSE = 1615497897U;
        static const AkUniqueID PLAY_UI_OPEN = 3905148667U;
        static const AkUniqueID PLAY_ZIPPER_LOOP = 3149298709U;
        static const AkUniqueID STOP_ALL = 452547817U;
        static const AkUniqueID STOP_AMBIENCE = 2477713992U;
        static const AkUniqueID STOP_ZIPPER_LOOP = 15513699U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID MENU_FILTER = 3284061781U;
        static const AkUniqueID MUSIC_GLOBAL = 2880921812U;
        static const AkUniqueID SFX_GLOBAL = 613679276U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID CHARACTER_SELECTION = 3112424181U;
        static const AkUniqueID FIGHT_SEQUENCE = 1782102031U;
        static const AkUniqueID MENU = 2607556080U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID REWARD_SCREEN = 1609258155U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID CHARACTER_SELECTION = 3112424181U;
        static const AkUniqueID FIGHT_SCENE = 649557016U;
        static const AkUniqueID GAME = 702482391U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MENU = 2607556080U;
        static const AkUniqueID MENU_FILTER_MUSIC = 2226205583U;
        static const AkUniqueID MUSIC_GLOBAL = 2880921812U;
        static const AkUniqueID REWARD_SCREEN = 1609258155U;
        static const AkUniqueID SFX_GLOBAL = 613679276U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
