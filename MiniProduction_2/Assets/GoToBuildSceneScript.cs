﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToBuildSceneScript : MonoBehaviour {

	public void GoToBuildScene()
	{
		AkSoundEngine.PostEvent("play_click", gameObject);
        AkSoundEngine.PostEvent("stop_all", gameObject);
		SaveLoad.Instance().SaveData();
		SceneController.Instance().LoadBuildTeam();
	}
	public void GoToMainMenu()
	{
        AkSoundEngine.PostEvent("stop_all", gameObject);
		AkSoundEngine.PostEvent("play_click", gameObject);
		SceneController.Instance().LoadMainMenu();
	}
}
