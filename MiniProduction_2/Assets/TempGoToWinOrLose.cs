﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempGoToWinOrLose : MonoBehaviour {

	public void WinGame()
	{
        FightController.Instance().GetEnemy().isDefeated = true;
		SceneController.Instance().LoadRewardScene();

	}
	public void LoseGame()
	{
		SceneController.Instance().LoadLoseScene();
	}
	public void GameOver()
	{
		SceneController.Instance().LoadGameOverScene();
	}
}
