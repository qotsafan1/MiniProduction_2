﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadModel : MonoBehaviour {

	[SerializeField]
	GameObject MaleModel;

	[SerializeField]
	GameObject FemaleModel;
	
	[SerializeField]
	GameObject BadMaleModel;

	[SerializeField]
	GameObject BadFemaleModel;
	
	public GameObject badGuyModel;
	public GameObject goodGuyModel;


	// Use this for initialization
	void Awake () {
		var player = FightController.Instance().GetPlayer();
		GameObject theModel;
		if(player.isMale) {
			theModel = MaleModel;
		} else {
			theModel = FemaleModel;
		}
		goodGuyModel = theModel;
		theModel.GetComponent<SleeveGameObjectScript>().sleeve = player;
		var anim = theModel.GetComponent<Animator>();

		anim.runtimeAnimatorController = Resources.Load("MotionMatchingController") as RuntimeAnimatorController;
		anim.applyRootMotion = true;   

		theModel.GetComponent<AnimationToggler>().enabled = true;
		theModel.GetComponent<FightAnimationController>().enabled = true;

		theModel.SetActive(true);


		var enemy = FightController.Instance().GetEnemy();
		GameObject theEnemyModel;
		if(enemy.isMale) {
			theEnemyModel = BadMaleModel;
		} else {
			theEnemyModel = BadFemaleModel;
		}
		badGuyModel = theEnemyModel;
		var animBad = theEnemyModel.GetComponent<Animator>();
		animBad.runtimeAnimatorController = Resources.Load("MotionMatchingController") as RuntimeAnimatorController;
		animBad.applyRootMotion = true;  

		theEnemyModel.SetActive(true);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
