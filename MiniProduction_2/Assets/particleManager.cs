﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particleManager : Manager<particleManager>
{
    [SerializeField]
     ParticleSystem particleEnemyHead;

    [SerializeField]
    ParticleSystem particleEnemyTorso;

    [SerializeField]
    ParticleSystem particleEnemyLower;

    [SerializeField]
    ParticleSystem particlePlayerHead;

    [SerializeField]
    ParticleSystem particlePlayerTorso;

    [SerializeField]
    ParticleSystem particlePlayerLower;

    public GameObject PlayerSleeve;
    public GameObject EnemySleeve;

    [SerializeField]
    LoadModel loadModel;


    private void Start()
    {
        
        PlayerSleeve = loadModel.goodGuyModel;
        EnemySleeve = loadModel.badGuyModel;

        particlePlayerHead = PlayerSleeve.GetComponent<ParticleSystemContainer>().particleHead;
        particlePlayerTorso = PlayerSleeve.GetComponent<ParticleSystemContainer>().particleTorso;
        particlePlayerLower = PlayerSleeve.GetComponent<ParticleSystemContainer>().particleLower;

        particleEnemyHead = EnemySleeve.GetComponent<ParticleSystemContainer>().particleHead;
        particleEnemyTorso = EnemySleeve.GetComponent<ParticleSystemContainer>().particleTorso;
        particleEnemyLower = EnemySleeve.GetComponent<ParticleSystemContainer>().particleLower;

    }

    public void EnemyHeadHit()
    {
        particleEnemyHead.Play();

    }
    public void EnemyTorsoHit()
    {
        particleEnemyTorso.Play();

    }
    public void EnemyLowerHit()
    {
        particleEnemyLower.Play();

    }

    public void PlayerHeadHit()
    {
        particlePlayerHead.Play();

    }
    public void PlayerTorsoHit()
    {
        particlePlayerTorso.Play();

    }
    public void PlayerLowerHit()
    {
        particlePlayerLower.Play();

    }






}
