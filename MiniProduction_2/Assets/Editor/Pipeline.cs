﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;



namespace UnityEditor
{
    public class Pipeline
    {
        [MenuItem("Pipeline/Build: Android")]

        public static void BuildAndroid()
        {
            if (Application.identifier.ToString().Contains("dev"))
            {
                PlayerSettings.productName = "development_" + Application.version.ToString();
            }
            else if (Application.identifier.ToString().Contains("test"))
            {
                PlayerSettings.productName = "testing_" + Application.version.ToString();
            }
            else
            {
                PlayerSettings.productName = "master_" + Application.version.ToString();
            }

            var result = BuildPipeline.BuildPlayer(new BuildPlayerOptions
            {
                locationPathName = Path.Combine(pathname, filename),
                scenes = EditorBuildSettings.scenes.Where(n => n.enabled).Select(n => n.path).ToArray(),
                target = BuildTarget.Android
            });
            Debug.Log(result);
        }

        public static string pathname
        {
            get
            {
                return "C:\\Users\\dadiu\\Dropbox\\DADIU_Team4\\020_MG2\\090_pipeline\\";
            }
        }

        public static string filename
        {
            get
            {
                if (Application.identifier.ToString().Contains("dev"))
                {
                    return ("development\\development_build" + "_" + Application.version.ToString() + ".apk");
                }
                else if (Application.identifier.ToString().Contains("test"))
                {
                    return ("testing\\testing_build" + "_" + Application.version.ToString() + ".apk");
                }
                else
                {
                    return ("master\\master_build" + "_" + Application.version.ToString() + ".apk");
                }
            }
        }

    }
}
