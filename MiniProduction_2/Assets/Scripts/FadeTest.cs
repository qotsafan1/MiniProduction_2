﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeTest : MonoBehaviour {
    Animator anim;
    AnimationLoader loader;
    bool fading = false;
    string state = "idle";
    bool debug = true;
    // Use this for initialization
    void Start () {
        loader = AnimationLoader.Instance();
        anim = GetComponent<Animator>();
        anim.Play("readyShort", 0, loader.FrameToTime("readyShort", 1));
    }
	
	// Update is called once per frame
	void Update () {
        AnimatorStateInfo animationState = anim.GetCurrentAnimatorStateInfo(0);
        if (state.Equals("idle"))
        {
            int frame = loader.TimeToFrame("readyShort", animationState.normalizedTime);
            if (frame > 55 && !fading)
            {
                fading = true;
                anim.CrossFade("readyShort", 0.1f, 0, loader.FrameToTime("readyShort", 1));
            }
            if (frame < 55)
                fading = false;
        }
        if (debug && Input.anyKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && state.Equals("idle"))
            {
                state = "Pull";
                int frame = loader.TimeToFrame("readyShort", animationState.normalizedTime);
                int nextFrame = loader.Transitions["readyShort"]["pullback"][frame];
                anim.CrossFade("pullback", 0.1f, 0, nextFrame);

            }
        }
        if (state.Equals("Pull"))
        {
            if (animationState.normalizedTime >= 1)
            {
                state = "punch";
                anim.CrossFade("punchGroin0", 0.1f, 0, loader.FrameToTime("punchGroin0", 1));
                fading = true;
            }
        } else if (state.Equals("punch"))
        {
            if (animationState.normalizedTime >= 0.9 && !fading)
            {
                state = "idle";
                anim.CrossFade("readyShort", 0.1f, 0, loader.FrameToTime("readyShort", 2));
                fading = true;
            }
            if (animationState.normalizedTime < 1)
                fading = false;
        }
    }
}
