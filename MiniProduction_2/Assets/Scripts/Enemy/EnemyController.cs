﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Manager<EnemyController>
{

    public int[] powerLevels = new int[] {60,100,150,200};

    public int GetPowerLevel(int currentLevel)
    {
        if (currentLevel >= powerLevels.Length)
        {
            return powerLevels[powerLevels.Length-1];
        }
        return powerLevels[currentLevel];
    }
}
