﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour {

    public EnemyScriptableObject Enemy;
    void Start()
    {
        //GenerateEnemy();
    }
    public void GenerateEnemy()
    {
        if (Enemy.isFirstTime)
        {
            Enemy.Randomize(0);
            Enemy.isFirstTime = false;
        }
        else if (Enemy.isDefeated)
        {
            Enemy.Randomize(Enemy.enemyLevel+1);
            
        }
        else
        {
            Enemy.ResetValues();
        }
    }
}
