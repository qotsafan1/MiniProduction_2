﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SaveLoad : Manager<SaveLoad> {

    public SleeveSOContainer sleeve;
  
  
    string path;

    private void Start()
    {
        path = Application.persistentDataPath + "/" + "data";
        
       
       

        //SaveData();  
    }


    public void SaveData()
    {
        return;

        string content;
        Debug.Log("game saved");

        for (int i = 0; i < sleeve.SOSleeve.Length; i++)
        {
            content = JsonUtility.ToJson(sleeve.SOSleeve[i], true);
            System.IO.File.WriteAllText(path+i+".json", content);
        }
       
    }

    public void LoadData()
    {
        return;
       // Debug.Log("game loaded");
        string content;

            for (int i = 0; i < sleeve.SOSleeve.Length; i++)
        {
            try
            {
                content = System.IO.File.ReadAllText(path + i + ".json");
                JsonUtility.FromJsonOverwrite(content, sleeve.SOSleeve[i]);
            }
            catch (Exception e)
            {
                //Debug.LogException(e, this);
                Debug.Log("no files to load");

            }

          

        }
        
    }

}
