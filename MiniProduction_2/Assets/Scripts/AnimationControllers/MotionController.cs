﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MotionController : MonoBehaviour {

	Animator anim;
	AnimatorStateInfo animationState;
	private AnimationState CurrentState = AnimationState.None;
	Vector3 OriginalPosition;
	Vector3 OriginalRotation;

	private bool debug = true;


	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		anim.speed = 1;
		CurrentState = AnimationState.ready;
		
		OriginalPosition = new Vector3(
			gameObject.transform.position.x, 
			gameObject.transform.position.y, 
			gameObject.transform.position.z);

		OriginalRotation = new Vector3(
			gameObject.transform.rotation.x, 
			gameObject.transform.rotation.y, 
			gameObject.transform.rotation.z);

		anim.Play("readyShort", 0, 0.01449275362f);
		
	}
	
	// Update is called once per frame
	void Update () {


		HandleInput();

		// FixPosition();
	}

	private void FixPosition() {
		transform.position = OriginalPosition;
		GetComponent<Transform>().eulerAngles = OriginalRotation;
	}

	private void HandleInput() {
		if (debug && Input.anyKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                anim.Play("pullback", 0, 0.01449275362f);
				CurrentState = AnimationState.pullback;
            } 
			else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                anim.Play("punchHead0", 0, 0.01449275362f);
				CurrentState = AnimationState.punch;
            }
        }
	}

}
