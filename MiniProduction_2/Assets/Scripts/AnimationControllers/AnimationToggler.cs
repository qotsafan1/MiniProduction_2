﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationToggler : MonoBehaviour {

    private Animator Anim;
    private Transform t;
    private AnimatorStateInfo AnimationStateInfo;
    private AnimationLoader Loader;
    public AnimationState State;
    float[] Rotation = new float[3];
    float[] Position = new float[3];
    private float FadeTime = 0.3f;
    private bool debug = true;

    private Transform LeftIndex1;
    private Transform LeftIndex2;
    private Transform LeftMiddle1;
    private Transform LeftMiddle2;
    private Transform LeftThumb;

    private Transform RightIndex1;
    private Transform RightIndex2;
    private Transform RightMiddle1;
    private Transform RightMiddle2;
    private Transform RightThumb;

    // Use this for initialization
    void Awake () {
        Loader = AnimationLoader.Instance();
        Anim = GetComponent<Animator>();

        t = GetComponent<Transform>();
        Rotation[0] = t.eulerAngles.x;
        Rotation[1] = t.eulerAngles.y;
        Rotation[2] = t.eulerAngles.z;

        Position[0] = t.position.x;
        Position[1] = t.position.y;
        Position[2] = t.position.z;
    }

    private void Start()
    {
        if (debug)
        {
            StartReady();
        }

        Transform hips = t.Find("RokokoGuy_root/RokokoGuy_Hips");
        if (hips == null)
            hips = t.Find("RokokoGuy_Hips");

        Transform lHand = hips.Find("RokokoGuy_Spine/RokokoGuy_Spine2/RokokoGuy_LeftShoulder/RokokoGuy_LeftArm/RokokoGuy_LeftForeArm/RokokoGuy_LeftHand");
        Transform rHand = hips.Find("RokokoGuy_Spine/RokokoGuy_Spine2/RokokoGuy_RightShoulder/RokokoGuy_RightArm/RokokoGuy_RightForeArm/RokokoGuy_RightHand");

        LeftIndex1 = lHand.Find("RokokoGuy_LeftIndex01");
        LeftIndex2 = LeftIndex1.Find("RokokoGuy_LeftIndex02");
        LeftMiddle1 = lHand.Find("RokokoGuy_LeftMiddle01");
        LeftMiddle2 = LeftMiddle1.Find("RokokoGuy_LeftMiddle02");
        LeftThumb = lHand.Find("RokokoGuy_LeftThumb01");

        RightIndex1 = rHand.Find("RokokoGuy_RightIndex005");
        RightIndex2 = RightIndex1.Find("RokokoGuy_RightIndex006");
        RightMiddle1 = rHand.Find("RokokoGuy_RightMiddle004");
        RightMiddle2 = RightMiddle1.Find("RokokoGuy_RightMiddle005");
        RightThumb = rHand.Find("RokokoGuy_RightThumb004");
    }

    // Update is called once per frame
    void Update () {
        AnimationStateInfo = Anim.GetCurrentAnimatorStateInfo(0);
        if (debug && Input.anyKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                PlayPullback();
            }
            else if (Input.GetKeyDown(KeyCode.Q))
            {
                PlayPunchHead();
            }
            else if (Input.GetKeyDown(KeyCode.A))
            {
                PlayPunchChest();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                PlayPunchGroin();
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                PlayHitHead();
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                PlayHitChest();
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                PlayHitGroin();
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                PlayMiss();
            }
        } else {
            switch (State)
            {
                case global::AnimationState.ready:
                    HandleReady();
                    break;
                case global::AnimationState.punchgroin:
                case global::AnimationState.punchchest:
                case global::AnimationState.punchhead:
                    HandlePunch();
                    break;
                case global::AnimationState.hitgroin:
                case global::AnimationState.hitchest:
                case global::AnimationState.hithead:
                    HandleHit();
                    break;
                case global::AnimationState.pullback:
                    HandlePullback();
                    break;
                case global::AnimationState.miss:
                    HandleMiss();
                    break;
            }
        }
        SmoothReset();
    }

    private void LateUpdate()
    {
        CloseHands();
    }

    private void CloseHands()
    {
        RightIndex1.Rotate(0, -90f, 0);
        RightIndex2.Rotate(0, -90f, 0);
        RightMiddle1.Rotate(0, -90f, 0);
        RightMiddle2.Rotate(0, -90f, 0);
        RightThumb.Rotate(0, 0, 90f);

        LeftIndex1.Rotate(0, 90f, 0);
        LeftIndex2.Rotate(0, 90f, 0);
        LeftMiddle1.Rotate(0, 90f, 0);
        LeftMiddle2.Rotate(0, 90f, 0);
        LeftThumb.Rotate(0, 0, 85f);
    }

    private void HandlePullback()
    {
        float time = GetTime();

        if (time > 0.31 && !IsFading())
        {
            Anim.speed = 0;
        }
    }

    private bool IsFading()
    {
        return Anim.GetAnimatorTransitionInfo(0).anyState;
        //return Faded < FadeTime + 1f;
    }

    public void HandleMiss()
    {
        float time = GetTime();
        if (time > 0.11 && !IsFading())
        {
            State = global::AnimationState.ready;
            Transition("miss", "readyShort");
        }
    }

    private void HandleReady()
    {
        int frame = Loader.TimeToFrame("readyShort", GetTime());
        if (frame > 55 && !IsFading())
        {
            Anim.CrossFade("readyShort", 0.1f, 0, Loader.FrameToTime("readyShort", 1));
        }
    }
    private void HandlePunch()
    {
        float timeToSwitch = 0f;
        switch (State)
        {
            case global::AnimationState.punchgroin:
                timeToSwitch = 0.13f;
                break;
            case global::AnimationState.punchchest:
                timeToSwitch = 0.96f;
                break;
            case global::AnimationState.punchhead:
                timeToSwitch = 0.93f;
                break;
        }
        if (AnimationStateInfo.normalizedTime >= timeToSwitch && !IsFading())
        {
            Transition(GetCurrentAnim(), "readyShort");
            State = global::AnimationState.ready;
        }
    }

    private void HandleHit()
    {
        float timeToSwitch = 0f;
        switch (State)
        {
            case global::AnimationState.hitgroin:
                timeToSwitch = 0.788f;
                break;
            case global::AnimationState.hitchest:
                timeToSwitch = 0.647f;
                break;
            case global::AnimationState.hithead:
                timeToSwitch = 0.738f;
                break;
        }
        if (AnimationStateInfo.normalizedTime >= timeToSwitch && !IsFading())
        {
            Transition(GetCurrentAnim(), "readyShort");
            State = global::AnimationState.ready;
        }
    }
    private void SmoothReset()
    {
        if (t.eulerAngles.x != Rotation[0] ||
            t.eulerAngles.y != Rotation[1] ||
            t.eulerAngles.z != Rotation[2])
        {
            float x = t.eulerAngles.x;
            float diffX = t.eulerAngles.x - Rotation[0];
            
            diffX = diffX > 180 ? diffX - 360 : (diffX < -180 ? diffX + 360 : diffX);
            
            if (!diffX.Equals(0f))
                x -= diffX / 100;

            float y = t.eulerAngles.y;
            float diffY = t.eulerAngles.y - Rotation[1];
            diffY = diffY > 180 ? diffY - 360 : (diffY < -180 ? diffX + 360 : diffY);
            if (!diffY.Equals(0f))
                y -= diffY / 100;

            float z = t.eulerAngles.z;
            float diffZ = t.eulerAngles.z - Rotation[2];
            diffZ = diffZ > 180 ? diffZ - 360 : (diffZ < -180 ? diffZ + 360 : diffZ);
            if (!diffX.Equals(0f))
                z -= diffZ / 100;

            t.eulerAngles = new Vector3(x, y, z);
        }

        if (t.position.x != Position[0] ||
            t.position.y != Position[1] ||
            t.position.z != Position[2])
        {
            float x = t.position.x;
            float diffX = t.position.x - Position[0];
            if (!diffX.Equals(0f))
                x -= diffX / 100;

            float y = t.position.y;
            float diffY = t.position.y - Position[1];
            if (!diffY.Equals(0f))
                y -= diffY / 100;

            float z = t.position.z;
            float diffZ = t.position.z - Position[2];
            if (!diffX.Equals(0f))
                z -= diffZ / 100;

            t.position = new Vector3(x, y, z);
        }
    }
    private void Transition(string from, string to)
    {
        Anim.speed = 1;
        int frame = Loader.TimeToFrame(from, 1);
        int startFrame = Loader.Transitions[from][to][frame];
        Anim.CrossFadeInFixedTime(to, FadeTime, 0, startFrame*0.033f);
    }
    private float GetTime()
    {
        return AnimationStateInfo.normalizedTime > 1 ? 1f : AnimationStateInfo.normalizedTime;
    }

    private string GetCurrentAnim()
    {
        switch (State)
        {
            case global::AnimationState.punchgroin:
                return "punchGroin0";
            case global::AnimationState.punchchest:
                return "punchTorso0";
            case global::AnimationState.punchhead:
                return "punchHead0";
            case global::AnimationState.hitgroin:
                return "hitGroin0";
            case global::AnimationState.hitchest:
                return "hitTorso0";
            case global::AnimationState.hithead:
                return "hitHead0";
            case global::AnimationState.ready:
                return "readyShort";
            case global::AnimationState.miss:
                return "miss";
        }
        return null;
    }

    private void FixHands()
    {

    }
    public void StartReady()
    {
        State = global::AnimationState.ready;
        Anim.Play("readyShort", 0, Loader.FrameToTime("readyShort", 1));
    }

    public void PlayPullback()
    {
        Transition("readyShort", "pullback");
        State = global::AnimationState.pullback;
    }

    public void PlayPunchGroin()
    {
        if (State.Equals(global::AnimationState.pullback))
        {
            Transition("pullback", "punchGroin0");
            State = global::AnimationState.punchgroin;
        }
    }
    public void PlayPunchChest()
    {
        if (State.Equals(global::AnimationState.pullback))
        {
            Transition("pullback", "punchTorso0");
            State = global::AnimationState.punchchest;
        }
    }
    public void PlayPunchHead()
    {
        if (State.Equals(global::AnimationState.pullback))
        {
            Transition("pullback", "punchHead0");
            State = global::AnimationState.punchhead;
        }
    }

    public void PlayHitHead()
    {
        string animFrom = GetCurrentAnim();
        if (!string.IsNullOrEmpty(animFrom))
        {
            Transition(animFrom, "hitHead0");
            State = global::AnimationState.hithead;
        }
    }
    public void PlayHitGroin()
    {
        string animFrom = GetCurrentAnim();
        if (!string.IsNullOrEmpty(animFrom))
        {
            Transition(animFrom, "hitGroin0");
            State = global::AnimationState.hitgroin;
        }
    }
    public void PlayHitChest()
    {
        string animFrom = GetCurrentAnim();
        if (!string.IsNullOrEmpty(animFrom))
        {
            Transition(animFrom, "hitTorso0");
            State = global::AnimationState.hitchest;
        }
    }

    public void PlayMiss()
    {
        State = global::AnimationState.miss;
        Transition("pullback", "miss");
    }
}
