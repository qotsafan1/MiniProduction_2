﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandUpAnimationController : MonoBehaviour {

    Animator anim;
    AnimatorStateInfo animationState;
    private AnimationState CurrentState = AnimationState.None;
    Vector3 OriginalPosition;
    Vector3 OriginalRotation;
    TurnOrder turnOrder;
    int TurnNumber;

    public bool shouldPunch;
    public Vector2 userInput;

    private bool debug = true;


    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.speed = 1;
        CurrentState = AnimationState.ready;
        anim.Play("fetalLoop", 0, 0.01449275362f);        
    }
}
