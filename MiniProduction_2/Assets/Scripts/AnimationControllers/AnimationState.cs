using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum AnimationState
{
    Standup, ready, pullback, hit, hithead, hitchest, hitgroin, punchhead, punchchest, punchgroin, punch, wakeup, miss, None
};