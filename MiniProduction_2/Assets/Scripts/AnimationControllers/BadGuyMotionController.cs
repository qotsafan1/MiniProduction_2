﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadGuyMotionController : Manager<BadGuyMotionController> 
{
	Animator anim;
	AnimatorStateInfo animationState;
	public  AnimationState CurrentState = AnimationState.None;
	Vector3 OriginalPosition;
	Vector3 OriginalRotation;

	public PlayerHand.HitSpot hitTarget;

	bool shouldBeHit;
	bool shouldPunch;
	TurnOrder turnOrder;
	int TurnNumber;
	public AnimationToggler animationToggler;
	bool hasPunched;

	// Use this for initialization
	void Start () {
		animationToggler = GetComponent<AnimationToggler>();
		hasPunched = false;
		shouldBeHit = false;
		shouldPunch = false;
		anim = GetComponent<Animator>();
		CurrentState = AnimationState.ready;
		anim.speed = 1;
		//anim.Play("readyShort", 0, 0.01449275362f);

		OriginalPosition = new Vector3(
			gameObject.transform.position.x, 
			gameObject.transform.position.y, 
			gameObject.transform.position.z);

		OriginalRotation = gameObject.transform.eulerAngles;

		turnOrder = TurnOrder.Instance();
		TurnNumber = turnOrder.GetCurrentTurnNumber();
		animationToggler.StartReady();
	}
	
		// Update is called once per frame
	void Update () {
		//CheckIfItsNewTurn();
		if (animationToggler.State == AnimationState.pullback && !hasPunched)
        {
            hasPunched = true;
        }
        if (hasPunched)
        {
            animationToggler.PlayPunchHead();
			hasPunched = false;
        }
		return;
		animationState = anim.GetCurrentAnimatorStateInfo(0);

		FixPosition();

		HandleTransitions();

	}
	void CheckIfItsNewTurn()
	{
		var currentTurnNumber = turnOrder.GetCurrentTurnNumber();
		if(currentTurnNumber != TurnNumber){
			if(turnOrder.GetCurrentTurn() == TurnOrder.Turn.Enemy) {
				animationToggler.PlayPullback();
			}
		}
		
		TurnNumber = currentTurnNumber;

	}
	private void HandleTransitions() {
		var currentTurnNumber = turnOrder.GetCurrentTurnNumber();

		// New turn
		if(currentTurnNumber != TurnNumber && CurrentState == AnimationState.ready){
			if(turnOrder.GetCurrentTurn() == TurnOrder.Turn.Enemy) {
				PlayPullback();
			}
		}
		else if (shouldBeHit)
		{
			shouldBeHit = false;
			PlayHitAnimation();
		} else if(shouldPunch) {
		 	PlayPunch(); //TODO: insert the input and motion match
			shouldPunch = false;
		}

		// Automatic transistions
		else if(CurrentState == AnimationState.punch && animationState.normalizedTime > 0.98F) {
			PlayReadyShort();
		}
		else if(CurrentState == AnimationState.punch && animationState.normalizedTime > 0.05F) {
			if (!FightAnimationController.Instance().IsGettingHit())
			{
				FightAnimationController.Instance().GetHit(PlayerHand.HitSpot.Head);
			}
		}


		else if(CurrentState == AnimationState.pullback && animationState.normalizedTime > 0.98F) {
			// TODO: This should be miss
			PlayReadyShort();			

		}
		// Loop Ready function
		else if(CurrentState == AnimationState.ready && animationState.normalizedTime > 0.44927536231F){
			PlayReadyShort();
		} 
		
		else if(CurrentState == AnimationState.hit && animationState.normalizedTime > 0.98F){
			PlayReadyShort();
		}

		TurnNumber = currentTurnNumber;
	}

	///////////////////////
	// Transition functions
	///////////////////////
	private void PlayReadyShort() {
		anim.CrossFade("readyShort", 0.0F, 0, 0.01449275362f, 0.1f);
		// anim.Play("readyShort", 0, 0.01449275362f);
		gameObject.transform.eulerAngles = OriginalRotation;
		anim.speed = 1;
		CurrentState = AnimationState.ready;
	}

	private void PlayPullback() {
		anim.CrossFade("pullback", 0.0F, 0, 0.01449275362f, 0.1f);
		CurrentState = AnimationState.pullback;
		anim.speed = 2;
	}

	private void PlayPunch() {
		//animationToggler.PlayPunchHead();
		animationToggler.PlayPullback();
		return;
		//anim.CrossFade("punchHead0", 0.0F, 0, 0.01449275362f, 0.1f);
		CurrentState = AnimationState.punch;
	}

	private void PlayHitAnimation()
	{
		string targetAnimation = "";
		switch (hitTarget)
		{
			case PlayerHand.HitSpot.Body:
			animationToggler.PlayHitChest();
			targetAnimation = "hitTorso0";
			break;
			case PlayerHand.HitSpot.Head:
			animationToggler.PlayHitHead();

			targetAnimation = "hitHead0";

			break;
			case PlayerHand.HitSpot.Legs:
			animationToggler.PlayHitGroin();

			targetAnimation = "hitGroin0";

			break;
			case PlayerHand.HitSpot.Miss:
			targetAnimation = "hitGroin0";
			
			break;
		}
		return;
		anim.CrossFade(targetAnimation, 0.0F, 0, 0.01449275362f, 0.1f);
		CurrentState = AnimationState.hit;

	}

	private void PlayPassout() {

	}
	
	///////////////////////
	// helper functions
	///////////////////////

	public bool IsPunching() {
		return CurrentState == AnimationState.punch;
	}

	private void FixPosition() {
		transform.position = OriginalPosition;
	}
	public bool IsGettingHit()
	{
		return CurrentState == AnimationState.hit;
	}
	public void StartPunching()
	{
		shouldPunch = true;
		PlayPunch();
	}

	public void GetHit(PlayerHand.HitSpot input)
	{
		hitTarget = input;
		shouldBeHit = true;
		PlayHitAnimation();
	}

}
