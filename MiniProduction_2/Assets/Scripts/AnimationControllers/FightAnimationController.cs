﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FightAnimationController : Manager<FightAnimationController> {
	
	Animator anim;
	AnimatorStateInfo animationState;
	public AnimationState CurrentState = AnimationState.None;
	Vector3 OriginalPosition;
	Vector3 OriginalRotation;
	TurnOrder turnOrder;
	int TurnNumber;
	public PlayerHand.HitSpot hitTarget;

	bool shouldBeHit;

	public bool shouldPunch;
	public Vector2 userInput;
	public PlayerHand.HitSpot userInputTarget;

	[SerializeField]
	BadGuyMotionController badGuyMotionController;

	private bool debug = true;

	public AnimationToggler animationToggler;
	// Use this for initialization
	void Awake () {


		// var player = FightController.Instance().GetPlayer();
		// GameObject newBody = Instantiate(player.sleeveModelPrefab);
		
		// GetComponent<AnimationToggler>().enabled = true;
		// GetComponent<FightAnimationController>().enabled = true;

		var anim = GetComponent<Animator>();

        // anim.runtimeAnimatorController = Resources.Load("MotionMatchingController") as RuntimeAnimatorController;
        // anim.applyRootMotion = true;   

		// newBody.transform.SetParent(transform.parent);

		animationToggler = GetComponent<AnimationToggler>();

		anim.speed = 1;
		CurrentState = AnimationState.ready;
		//anim.Play("readyShort", 0, 0.01449275362f);


		OriginalPosition = new Vector3(
			gameObject.transform.position.x, 
			gameObject.transform.position.y, 
			gameObject.transform.position.z);

		OriginalRotation = gameObject.transform.eulerAngles;
		// animationToggler.StartReady();
		// var theTransform = GetComponent<Transform>();
		// OriginalRotation = new Quaternion(theTransform.rotation);
		
	}
	private void Start() {
		TurnNumber = TurnOrder.Instance().GetCurrentTurnNumber();
		animationToggler.StartReady();
	}	
	// Update is called once per frame
	void Update () {
		CheckIfItsNewTurn();
		return;
		animationState = anim.GetCurrentAnimatorStateInfo(0);

		FixPosition();

		HandleTransitions();


		if(debug) {
			HandleInput();
		}

	}

	void CheckIfItsNewTurn()
	{
		var currentTurnNumber = TurnOrder.Instance().GetCurrentTurnNumber();
		if(currentTurnNumber != TurnNumber){
			if(TurnOrder.Instance().GetCurrentTurn() == TurnOrder.Turn.Player) {
				animationToggler.PlayPullback();
			}
		}
		
		TurnNumber = currentTurnNumber;

	}

	private void HandleTransitions() {
		var currentTurnNumber = turnOrder.GetCurrentTurnNumber();
		// Handle events or Inputs

		// New turn
		if(currentTurnNumber != TurnNumber){
			if(turnOrder.GetCurrentTurn() == TurnOrder.Turn.Player) {
				PlayPullback();
			} else {
				PlayReadyShort();
			}
		}
		else if (shouldBeHit)
		{
			shouldBeHit = false;
			PlayHitAnimation();
		}
		// activate punch
		else if(shouldPunch) {
		 	PlayPunch(); //TODO: insert the input and motion match
			shouldPunch = false;
		}
		 

		// Automatic transistions
		else if(CurrentState == AnimationState.punch && animationState.normalizedTime > 0.98F) {
			PlayReadyShort();
		}

		else if (CurrentState == AnimationState.punch && animationState.normalizedTime > 0.05F){
			if (!badGuyMotionController.IsGettingHit())
			{
				badGuyMotionController.GetHit(userInputTarget);
			
			}
		}

		else if(CurrentState == AnimationState.pullback && TurnOrder.Instance().GetCurrentTime() > TurnOrder.Instance().GetTurnTime() *0.98F) {
			// TODO: This should be miss
			PlayReadyShort();
		}
		// Loop Ready function
		else if(CurrentState == AnimationState.ready && animationState.normalizedTime > 0.44927536231F){
			PlayReadyShort();
		}
		else if(CurrentState == AnimationState.hit && animationState.normalizedTime > 0.98F){
			PlayReadyShort();
		}

		TurnNumber = currentTurnNumber;
	}

	///////////////////////
	// Transition functions
	///////////////////////
	private void PlayReadyShort() {
		gameObject.transform.eulerAngles = OriginalRotation;
		anim.CrossFade("readyShort", 0.0F, 0, 0.01449275362f, 0.1f);
		anim.speed = 1;
		CurrentState = AnimationState.ready;
	}

	private void PlayPullback() {
		anim.CrossFade("pullback", 0.0F, 0, 0.01449275362f, 0.1f);
		CurrentState = AnimationState.pullback;
		anim.speed = 2;
	}

	private void PlayPunch() {
		string targetAnimation = "";
		switch (userInputTarget)
		{
			case PlayerHand.HitSpot.Body:
			targetAnimation = "punchTorso0";
			animationToggler.PlayPunchChest();
			break;
			case PlayerHand.HitSpot.Head:
			targetAnimation = "punchHead0";
			animationToggler.PlayPunchHead();

			break;
			case PlayerHand.HitSpot.Legs:
			targetAnimation = "punchGroin0";
			animationToggler.PlayPunchGroin();

			break;
			case PlayerHand.HitSpot.Miss:
			targetAnimation = "punchGroin0";
			
			break;
		}
		return;
		anim.CrossFade(targetAnimation, 0.0F, 0, 0.01449275362f, 0.1f);
		CurrentState = AnimationState.punch;
		TurnOrder.Instance().PlayerAttacked();
	}
	private void PlayHitAnimation()
	{
		animationToggler.PlayHitHead();
		return;
		string targetAnimation = "";
		switch (hitTarget)
		{
			case PlayerHand.HitSpot.Body:
			targetAnimation = "hitTorso0";
			break;
			case PlayerHand.HitSpot.Head:
			targetAnimation = "hitHead0";

			break;
			case PlayerHand.HitSpot.Legs:
			targetAnimation = "hitGroin0";

			break;
			case PlayerHand.HitSpot.Miss:
			targetAnimation = "hitGroin0";
			
			break;
		}
		anim.CrossFade(targetAnimation, 0.0F, 0, 0.01449275362f, 0.1f);
		CurrentState = AnimationState.hit;

	}
	public void PlayMiss()
	{
		animationToggler.PlayMiss();
	}

	private void PlayPassout() {

	}

	
	///////////////////////
	// helper functions
	///////////////////////

	public void GetHit(PlayerHand.HitSpot input)
	{
		hitTarget = input;
		shouldBeHit = true;
		PlayHitAnimation();

	}
	public bool IsPunching() {
		return CurrentState == AnimationState.punch;
	}
	public bool IsGettingHit()
	{
		return CurrentState == AnimationState.hit;
	}

	private void FixPosition() {
		transform.position = OriginalPosition;
	}
	
	public void SetUserInput(Vector2 input) {
		shouldPunch = true;
		userInput = input;
		PlayPunch();
	}
	public void SetUserInput(PlayerHand.HitSpot input) {
		userInputTarget =input;
		shouldPunch = true;
		PlayPunch();

	}


	private void HandleInput() {
		if (debug && Input.anyKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                PlayPullback();
            } 
			else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                PlayPunch();
            }
        }
	}
}
