﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LooseSoundController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AkSoundEngine.PostEvent( "play_crowd_lose", gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
