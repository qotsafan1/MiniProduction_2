﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAiming : MonoBehaviour,ITimeAction {
	Vector2 fingerStartPosition;
	public Vector2 direction;
	float deadzone = 10f;
	bool isAiming;
	bool gotDirection;

	[SerializeField]
	public UnityEvent attack;
	

    public bool TimeStep(float deltaTime)
    {	
		
        if (TurnOrder.Instance().GetCurrentTurn() != TurnOrder.Turn.Player) 
		{
			ResetInputRegistration();
			return false;
		}
		InputObserver();
		return false;
    }

	void ResetInputRegistration()
	{
		fingerStartPosition = Vector2.zero;
		isAiming = false;
		gotDirection = false;
		AimingArrow.Instance().DeactivateArrow();
	}

	
	

	void ActivatePunch()
	{
		//Debug.Log("Activating Punch");
		AimingArrow.Instance().FlyArrowInDirection(direction);
		attack.Invoke();
		//fightAnimationController.shouldPunch = true;
		FightAnimationController.Instance().SetUserInput(direction);
	}
    void Start()
	{
		FightUpdateLoop.Instance().SubscribeToUpdateLoop(this);
		isAiming = false;
		gotDirection = false;
	}
	
	void InputObserver()
	{
		if (Input.touchCount == 0) 
		{
			if (isAiming)
			{
				if (gotDirection)
				{
					ActivatePunch();
				}
				ResetInputRegistration();
			}
			return;
		}
		if (!isAiming)
		{
			//First input contact
			isAiming = true;
			fingerStartPosition = Input.touches[0].position;
		}
		float distanceForFinger = Vector2.Distance(Input.touches[0].position,fingerStartPosition);
		if (distanceForFinger < deadzone) 
		{
			gotDirection = false;
			return;
		}
		//Debug.Log("ViewPort: " + Camera.main.ScreenToViewportPoint( Input.touches[0].position) + " Original: " + Input.touches[0].position);
		//Start the trajectory
		direction = fingerStartPosition- Input.touches[0].position ;
		gotDirection = true;
		//ShowAimingArrow();
		AimingArrow.Instance().ActivateArrow();
		//AimingArrow.Instance().MoveArrow(direction);
		AimingArrow.Instance().MoveArrow(direction,distanceForFinger);

	}
}
