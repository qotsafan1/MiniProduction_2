﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FightController : Manager<FightController> {

	[SerializeField]
	SleeveScriptableObject[] fighters;
	SleeveScriptableObject player;
	[SerializeField]
	public EnemyScriptableObject enemy;

	public bool isRunning;
	float currentCountDownTimer = 0;
	int currentCountDownint = 0;
	[SerializeField]
	Text countDownText; 
	
	// Use this for initialization
	void Awake () {
	   
	   isRunning = false;
	   
	}
	void Start()
	{
		transform.parent = GameObject.Find("ParentForDestroy").transform;
		AkSoundEngine.PostEvent("play_blood_lust",gameObject);		
		AkSoundEngine.PostEvent("play_crowd",gameObject);
	}
	public EnemyScriptableObject GetEnemy()
	{
		return enemy;
	}
	public SleeveScriptableObject GetPlayer()
	{
		if (player == null)
		{
			player = FindFighter();
		}
		return player;
	}
	public SleeveScriptableObject FindFighter()
	{
		for (int i = 0; i < fighters.Length; i++)
		{
			if (fighters[i].isReady)
			{
				return fighters[i];
			}
		}
		SleeveScriptableObject tempSleeve = new SleeveScriptableObject();
		tempSleeve.Randomize(100);
		return tempSleeve;
	}
	public bool GetDoesPlayerHaveActiveSleeves()
	{
		for (int i = 0; i < fighters.Length; i++)
		{
			if (fighters[i].isActive)
			{
				return true;
			}
		}
		return false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isRunning) {return;}
		GameObject mainMenu = GameObject.Find("MainButtons");
		if (mainMenu != null)
		{
			if (mainMenu.activeSelf)
			{
				return;
			}
		}
		currentCountDownTimer += Time.deltaTime;
		if (currentCountDownTimer > 1)
		{
			currentCountDownint++;
			currentCountDownTimer = 0;
			CountDown();
		}
	}
	void CountDown()
	{
		switch ((int)currentCountDownint)
		{
			case 2:
			Debug.Log(3);
			//MIKKEL START LYD
			Write("3");
			break;
			
			case 3:
			Debug.Log(2);
			Write("2");
			break;
			case 4:
			Debug.Log(1);
			Write("1");
			break;
			case 5:
			Debug.Log("GO");
			StartFight();
			break;
			 
		}
	}
	void Write(string textToWrite)
	{
		if (countDownText == null) {return;}
		countDownText.text = textToWrite;
		//CancelInvoke("MakeWriteBigger");
		countDownText.fontSize = 200;
		InvokeRepeating("MakeWriteBigger",Time.fixedDeltaTime,Time.fixedDeltaTime);
	}

	void MakeWriteBigger()
	{
		countDownText.fontSize = countDownText.fontSize+  (int)(100.0f * Time.fixedDeltaTime);
	}
	void StartFight()
	{
		FightUpdateLoop.Instance().StartUpdateLoop();
		TurnOrder.Instance().StartTurnSystem();
		AkSoundEngine.PostEvent("play_announcer_fight",gameObject);
		isRunning = false;
		Write("");
		CancelInvoke("MakeWriteBigger");
	}

	public bool GetIsRunning()
	{
		return isRunning;
	}

}
