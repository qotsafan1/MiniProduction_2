﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemContainer : MonoBehaviour {

	[SerializeField]

	public ParticleSystem particleHead;
	[SerializeField]

	public ParticleSystem particleTorso;
	[SerializeField]

	public ParticleSystem particleLower;

}
