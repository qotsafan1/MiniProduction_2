﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITimeAction {
	bool TimeStep(float deltaTime);
}
