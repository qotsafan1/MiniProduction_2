﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightUpdateLoop : Manager<FightUpdateLoop> {
	public enum UpdateLoopStatus {Running, Paused};
	List<ITimeAction> currentTimeActions;
	List<ITimeAction> systemTimeActions;
	List<ITimeAction> waitcurrentTimeActions;
	List<ITimeAction> waitsystemTimeActions;
	List<ITimeAction> deleteITimeActions;
	List<ITimeAction> deleteSystemITimeActions;

	UpdateLoopStatus currentUpdateLoopStatus;
    MainMenuScript mainMenu;
    

	// Use this for initialization
	void Awake () {
        currentUpdateLoopStatus = UpdateLoopStatus.Paused;
		SetupITimeActionLists();
	}
    void Start()
    {
		transform.parent = GameObject.Find("ParentForDestroy").transform;
        try{
            mainMenu = GameObject.Find("MainMenu").GetComponent<MainMenuScript>();
        } catch{
            
        }
    }
    
	void SetupITimeActionLists()
	{
		currentTimeActions = new List<ITimeAction>();
		systemTimeActions = new List<ITimeAction>();
		waitcurrentTimeActions = new List<ITimeAction>();
        waitsystemTimeActions = new List<ITimeAction>();
		deleteITimeActions = new List<ITimeAction>();
		deleteSystemITimeActions = new List<ITimeAction>();
	}
	
	// Update is called once per frame
	void Update () {
		if (currentUpdateLoopStatus == UpdateLoopStatus.Running)
		{
			RunUpdateLoop(Time.deltaTime);
		}
        if (FightController.Instance().GetIsRunning())
        {
            return;
        }
        if (mainMenu == null) {return;}
        if (mainMenu.IsMainMenuOpen())
        {
            currentUpdateLoopStatus = UpdateLoopStatus.Paused;
        } else {
            currentUpdateLoopStatus = UpdateLoopStatus.Running;

        }
	}

	void RunUpdateLoop(float deltaTime)
	{
		if (currentTimeActions.Count != 0 || systemTimeActions.Count != 0)
            {
                //Debug.Log("Got somekind of action" + systemTimeActions.Count);
                foreach (ITimeAction time in currentTimeActions)
                {
                    //Check if any action is done. If it is, stop time
                    if (time.TimeStep(deltaTime))
                    {
                        deleteITimeActions.Add(time);
                    }
                }
                foreach(ITimeAction time in systemTimeActions)
                {
                    if (time.TimeStep(deltaTime))
                    {
                        deleteSystemITimeActions.Add(time);
                    }
                }
                if (deleteITimeActions.Count != 0)
                {
                    foreach (ITimeAction time in deleteITimeActions)
                    {
                        currentTimeActions.Remove(time);
                    }
					deleteITimeActions.Clear();
                }
				if (deleteSystemITimeActions.Count != 0)
                {
                    foreach (ITimeAction time in deleteSystemITimeActions)
                    {
                        systemTimeActions.Remove(time);
                    }
					deleteSystemITimeActions.Clear();
                }
            }


            //Add the waiting time actions
            if (waitcurrentTimeActions.Count != 0)
            {
                foreach (ITimeAction item in waitcurrentTimeActions)
                {
                    currentTimeActions.Add(item);
                }
                waitcurrentTimeActions.Clear();
            }
            if (waitsystemTimeActions.Count != 0)
            {
                foreach (ITimeAction item in waitsystemTimeActions)
                {
                    systemTimeActions.Add(item);
                }
                waitsystemTimeActions.Clear();
            }
	}
	public void SubscribeToUpdateLoop(ITimeAction newITimeAction)
	{
		waitcurrentTimeActions.Add(newITimeAction);
        //Debug.Log(newITimeAction.GetType() +" Subscribed");
	}
	//Should not be used return true in TimeStep instead
	public void UnsubscribeToUpdateLoop(ITimeAction oldITimeAction)
	{
		deleteITimeActions.Add(oldITimeAction);
	}
    public void StartUpdateLoop()
    {
        currentUpdateLoopStatus = UpdateLoopStatus.Running;
    }
    public void PauseUpdateLoop()
    {
        currentUpdateLoopStatus = UpdateLoopStatus.Paused;

    }
}
