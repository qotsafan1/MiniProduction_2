﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLoseFightBehaviour : MonoBehaviour,ITimeAction {
    public bool TimeStep(float deltaTime)
    {
        if (FightController.Instance().GetPlayer().currentHealth <1)
		{
			//Player loses
			Invoke("EnemyWon",2.0f);
			//Do death animation on player
			TurnOrder.Instance().SetTurnToNoOne();
			FightController.Instance().GetPlayer().isActive = false;
			FightUpdateLoop.Instance().UnsubscribeToUpdateLoop(this);


		} else if (FightController.Instance().GetEnemy().currentHealth < 1)
		{
			//Enemy Loses
			FightController.Instance().GetEnemy().isDefeated = true;
			Invoke("PlayerWon",2.0f);
			//Do death animation on enemy
			TurnOrder.Instance().SetTurnToNoOne();
			FightUpdateLoop.Instance().UnsubscribeToUpdateLoop(this);

			
		}
		return false;
    }
	

    // Use this for initialization
    void Start () {
		FightUpdateLoop.Instance().SubscribeToUpdateLoop(this);
	}
	
	void PlayerWon()
	{
		SceneController.Instance().LoadRewardScene();
	}
	void EnemyWon()
	{
		if (FightController.Instance().GetDoesPlayerHaveActiveSleeves())
		{
			SceneController.Instance().LoadLoseScene();
		} else {
			SceneController.Instance().LoadGameOverScene();
		}
	}
}
