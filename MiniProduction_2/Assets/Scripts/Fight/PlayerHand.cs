﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHand : MonoBehaviour {
	public enum HitSpot {Head,Body,Legs,Miss};

	public float maxDistance;
	[SerializeField]
	LayerMask layerMask;

	[SerializeField]
	PlayerAiming playerAiming;
	void Start()
	{
		if (maxDistance == 0)
		{
			maxDistance = 10.0f;
		}
		if (layerMask == 0)
		{
			layerMask = 1;
		}
	}
	public void OnCollisionEnter(Collision other)
	{
		Contact(other.gameObject);
	}
	public void OnTriggerEnter(Collider other)
	{
		Contact(other.gameObject);
	}

	public void RaycastForConnection()
	{
		bool twoD = true;
		Vector2 direciton = playerAiming.direction;

		if (twoD)
		{
			Ray2D ray = new Ray2D(AimingArrow.Instance().centralPosition,direciton);
			RaycastHit2D temp = Physics2D.Raycast(AimingArrow.Instance().centralPosition,playerAiming.direction,3000.0f,layerMask);
			if (temp.collider != null)
			{
				Debug.Log(temp.collider.gameObject);
				//Hit
				Contact(temp.collider.gameObject);
			} else {
				//MISS you so bad!
				FightAnimationController.Instance().PlayMiss();
			}
		} else {
			//maybe ransform ray to 3rd dimensions
			Ray ray = new Ray(transform.position,direciton);
			RaycastHit hit;
			Debug.DrawRay(ray.origin,ray.direction);
			Physics.Raycast(ray,out hit,maxDistance,layerMask);
			
			if (hit.collider != null)
			{
				//Hit
				Contact(hit.collider.gameObject);
			} else {
				//MISS you so bad!
				FightAnimationController.Instance().SetUserInput(HitSpot.Miss);
			}
		}
		
	}
	void Contact(GameObject connectedObject)
	{
		if (connectedObject.tag == "Enemy")
		{
			//Debug.Log((connectedObject.name.StartsWith("Body")));
			if (connectedObject.name == "Head")
			{
				DamageController.Instance().GiveDamagerToEnemy(1.5f);
				Debug.Log("Head shot!");
				FightAnimationController.Instance().SetUserInput(HitSpot.Head);
				BadGuyMotionController.Instance().GetHit(HitSpot.Head);
                particleManager.Instance().EnemyHeadHit();

			} else if (connectedObject.name.StartsWith("Body"))
			{
				DamageController.Instance().GiveDamagerToEnemy(1.0f);
				FightAnimationController.Instance().SetUserInput(HitSpot.Body);
				BadGuyMotionController.Instance().GetHit(HitSpot.Body);

				Debug.Log("Body shot!");
                particleManager.Instance().EnemyTorsoHit();

            } else if (connectedObject.name.StartsWith("Legs"))
			{
				DamageController.Instance().GiveDamagerToEnemy(0.75f);
				TurnOrder.Instance().MoveNextEnemyTurnXSpacesBack(5);
				FightAnimationController.Instance().SetUserInput(HitSpot.Legs);
				BadGuyMotionController.Instance().GetHit(HitSpot.Legs);

				Debug.Log("Leg shot!");
                particleManager.Instance().EnemyLowerHit();

            }
		}
	}
}
