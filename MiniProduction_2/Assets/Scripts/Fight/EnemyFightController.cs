﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFightController : MonoBehaviour, ITimeAction {
	bool wasLastTurnEnemy;
	int TurnNumber;
    public bool TimeStep(float deltaTime)
    {

		var currentTurnNumber = TurnOrder.Instance().GetCurrentTurnNumber();
		if(currentTurnNumber != TurnNumber){
			if(TurnOrder.Instance().GetCurrentTurn() == TurnOrder.Turn.Enemy) {
				StartAttackAnimation();
			}
		}
		TurnNumber = currentTurnNumber;
		return false;

        if (TurnOrder.Instance().GetCurrentTurn() != TurnOrder.Turn.Enemy) 
		{
			if (wasLastTurnEnemy == true)
			{
				//Attack();
			}
			wasLastTurnEnemy = false;
			return false;
		}
		if (!wasLastTurnEnemy)
		{
			
		}

		wasLastTurnEnemy = true;
		return false;
    }

    // Use this for initialization
    void Start () {
		FightUpdateLoop.Instance().SubscribeToUpdateLoop(this);
		wasLastTurnEnemy = false;
		TurnNumber = -1;
	}
	void StartAttackAnimation()
	{
		//What ever you need
		BadGuyMotionController.Instance().StartPunching();
		Invoke("Attack",TurnOrder.Instance().GetTurnTime() / 3.0f);
	}
	
	void Attack()
	{
		DamageController.Instance().GiveDamageToPlayer();
		FightAnimationController.Instance().GetHit(PlayerHand.HitSpot.Body);
	}
}
