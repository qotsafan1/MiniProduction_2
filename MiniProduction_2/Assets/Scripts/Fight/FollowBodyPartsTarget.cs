﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBodyPartsTarget : MonoBehaviour {

	[SerializeField]
	public Transform Head;
	[SerializeField]
	public Transform BodyStomach;
	[SerializeField]
	public Transform BodyUpper;
	[SerializeField]
	public Transform Legs;
	[SerializeField]
	public Transform LegsLower;

	public Transform GetBodyPart(string item)
	{
		switch (item)
		{
			case "Head":
			return Head;
			case "BodyStomach":
			return BodyStomach;
			case "BodyUpper":
			return BodyUpper;
			case "Legs":
			return Legs;
			case "LegsLower":
			return LegsLower;
		}
		return LegsLower;
	}


}
