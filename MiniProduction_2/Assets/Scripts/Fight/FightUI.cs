﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightUI : MonoBehaviour,ITimeAction {
	[SerializeField]
	Image timer;
	[SerializeField]
	Image timerColor;
	[SerializeField]
	Image playerHealthBar;
	[SerializeField]
	Image enemyHealthBar;
	[SerializeField]
	Image playerDamageBar;
	[SerializeField]
	Image enemyDamageBar;

	[SerializeField]
	Sprite playerImage;
	[SerializeField]
	Sprite enemyImage;
	[SerializeField]
	Sprite playerTimer;
	[SerializeField]
	Sprite enemyTimer;

	[SerializeField]
	Transform turnObjectsParent;

	Image[] turnOrderObjects;
    public bool TimeStep(float deltaTime)
    {
		SetUpUI();
        UpdateUI();
		return false;
    }
	void Awake()
	{
		turnOrderObjects = turnObjectsParent.GetComponentsInChildren<Image>();
		for (int i = 0; i < turnOrderObjects.Length; i++)
		{
			turnOrderObjects[i].gameObject.SetActive(false);
		}
		timer.gameObject.SetActive(false);
		timerColor.gameObject.SetActive(false);

	}

    // Use this for initialization
    void Start () {
		FightUpdateLoop.Instance().SubscribeToUpdateLoop(this);
	}
	
	// Update is called once per frame
	void UpdateUI () {
		UpdateTimer();
		UpdateHealthBars();
		UpdateTurnOrder();
	}

	void UpdateTimer()
	{
		float fillamount = TurnOrder.Instance().GetCurrentTime() / TurnOrder.Instance().GetTurnTime();
		timer.fillAmount = fillamount;

		if (TurnOrder.Instance().GetCurrentTurn() == TurnOrder.Turn.Player)
		{
			timerColor.sprite = playerTimer;
		} else {
			timerColor.sprite = enemyTimer;

		}
	}
	void UpdateHealthBars()
	{
		float fillamount = (float)FightController.Instance().GetPlayer().currentHealth / (float)FightController.Instance().GetPlayer().health;
		float damageAmount = playerHealthBar.fillAmount - fillamount;

		if (playerHealthBar.fillAmount != fillamount)
		{	
			playerDamageBar.gameObject.SetActive(true);
			//player damager occured
			//find the previous fillamount. thats the x position
			//set size according to the damage fillamount.
			Vector2 damageSize = playerHealthBar.GetComponent<RectTransform>().sizeDelta;
			damageSize.x *= damageAmount;
			//Tag den fulde størrelse, plus fillamount og halvdelen af damage amout. Set det som ny position.
			Vector2 damagePosition = playerHealthBar.GetComponent<RectTransform>().position;
			damagePosition.x -= ((playerHealthBar.GetComponent<RectTransform>().sizeDelta.x) /2);
			damagePosition.x += (playerHealthBar.GetComponent<RectTransform>().sizeDelta.x *fillamount) + ((playerHealthBar.GetComponent<RectTransform>().sizeDelta.x *damageAmount)/2);
			//Debug.Log("Previous position: " + playerDamageBar.GetComponent<RectTransform>().position + " and size: " + playerDamageBar.GetComponent<RectTransform>().sizeDelta );
			playerDamageBar.GetComponent<RectTransform>().position = damagePosition; 
			playerDamageBar.GetComponent<RectTransform>().sizeDelta = damageSize;
			//Debug.Log("New position: " + playerDamageBar.GetComponent<RectTransform>().position + " and size: " + playerDamageBar.GetComponent<RectTransform>().sizeDelta );
			StartRemovingDamageIndicator("Player");
		}
		playerHealthBar.fillAmount = fillamount;
		fillamount = (float)FightController.Instance().GetEnemy().currentHealth / (float)FightController.Instance().GetEnemy().health;
		//Debug.Log(fillamount + " currentHealt: " + FightController.Instance().GetEnemy().currentHealth + " total health: " + FightController.Instance().GetEnemy().health);
		damageAmount = enemyHealthBar.fillAmount - fillamount;
		if (enemyHealthBar.fillAmount != fillamount)
		{	
			enemyDamageBar.gameObject.SetActive(true);
			//player damager occured
			//find the previous fillamount. thats the x position
			//set size according to the damage fillamount.
			Vector2 damageSize = enemyHealthBar.GetComponent<RectTransform>().sizeDelta;
			damageSize.x *= damageAmount;
			//Tag den fulde størrelse, plus fillamount og halvdelen af damage amout. Set det som ny position.
			Vector2 damagePosition = enemyHealthBar.GetComponent<RectTransform>().position;
			damagePosition.x += ((enemyHealthBar.GetComponent<RectTransform>().sizeDelta.x) /2);
			damagePosition.x -= (enemyHealthBar.GetComponent<RectTransform>().sizeDelta.x *fillamount) + ((enemyHealthBar.GetComponent<RectTransform>().sizeDelta.x *damageAmount)/2);
			//Debug.Log("Previous position: " + enemyDamageBar.GetComponent<RectTransform>().position + " and size: " + enemyDamageBar.GetComponent<RectTransform>().sizeDelta );
			enemyDamageBar.GetComponent<RectTransform>().position = damagePosition; 
			enemyDamageBar.GetComponent<RectTransform>().sizeDelta = damageSize;
			//Debug.Log("New position: " + enemyDamageBar.GetComponent<RectTransform>().position + " and size: " + enemyDamageBar.GetComponent<RectTransform>().sizeDelta );
			StartRemovingDamageIndicator("Enemy");
		}
		enemyHealthBar.fillAmount = fillamount;
	}
	void StartRemovingDamageIndicator(string who)
	{
		if (who == "Player")
		{
			CancelInvoke("DeminishPlayerDamageIndicator");
			InvokeRepeating("DeminishPlayerDamageIndicator",1.0f,Time.fixedDeltaTime);
		} else {
			CancelInvoke("DeminishEnemyDamageIndicator");

			InvokeRepeating("DeminishEnemyDamageIndicator",1.0f,Time.fixedDeltaTime);
		}
	}

	void DeminishPlayerDamageIndicator()
	{
		playerDamageBar.fillAmount = playerDamageBar.fillAmount - Time.fixedDeltaTime;
		if (playerDamageBar.fillAmount <= 0.0f)
		{
			playerDamageBar.gameObject.SetActive(false);
			playerDamageBar.fillAmount = 1.0f;
			CancelInvoke("DeminishPlayerDamageIndicator");
		}
	}
	void DeminishEnemyDamageIndicator()
	{
		enemyDamageBar.fillAmount = enemyDamageBar.fillAmount - Time.fixedDeltaTime;
		if (enemyDamageBar.fillAmount <= 0.0f)
		{
			enemyDamageBar.gameObject.SetActive(false);
			enemyDamageBar.fillAmount = 1.0f;
			CancelInvoke("DeminishEnemyDamageIndicator");
		}

	}
	public void SetUpUI()
	{
		for (int i = 0; i < turnOrderObjects.Length; i++)
		{
			turnOrderObjects[i].gameObject.SetActive(true);
		}
		timer.gameObject.SetActive(true);
		timerColor.gameObject.SetActive(true);

	}

	void UpdateTurnOrder()
	{
		int[] tempOrder =  TurnOrder.Instance().GetNextXTurns(turnOrderObjects.Length);
		


		for (int i = 0; i < tempOrder.Length; i++)
		{
			Color tempColor = Color.yellow;
			//Enemy color
			if (tempOrder[i] == 0)
			{
				//player
				turnOrderObjects[i].sprite = playerImage;
			} else {
				tempColor = Color.blue;
				turnOrderObjects[i].sprite = enemyImage;

				//Enemy
			}
			
			//turnOrderObjects[i].color = tempColor;
		}
	}
}
