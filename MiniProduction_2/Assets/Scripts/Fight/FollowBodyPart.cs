﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBodyPart : MonoBehaviour {
	[SerializeField]
	Transform bodyPartToFollow;
	Vector2 startPosition;
	RectTransform rect;
	[SerializeField]
	LoadModel loadModel;
	// Use this for initialization
	void Start () {
		rect = GetComponent<RectTransform>();
		startPosition = transform.position;
		bodyPartToFollow = loadModel.badGuyModel.GetComponent<FollowBodyPartsTarget>().GetBodyPart(gameObject.name);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 newPosition = Camera.main.WorldToScreenPoint( bodyPartToFollow.position);
		newPosition.z = 0;
		transform.position = newPosition;
	}
}
