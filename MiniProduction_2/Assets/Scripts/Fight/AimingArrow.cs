﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AimingArrow : Manager<AimingArrow>,ITimeAction {

	[SerializeField]
	Image arrow;
	[SerializeField]
	Transform centralPositionTransform;
	bool isShowing;
	Vector2 startPosition;
	Quaternion startRotation;
	public Vector2 centralPosition;

	Vector3 startScale;
	float minDistance = 100.0f;
	float maxDistance;

	bool isFlying;
	Vector2 flyingDirection;
	float flyingSpeed = 80;
	// Use this for initialization
	void Start () {
		startPosition = arrow.transform.position;
		startRotation = arrow.transform.rotation;
		centralPosition = centralPositionTransform.position;
		maxDistance = Vector3.Distance(arrow.transform.position, centralPositionTransform.position);
		transform.parent = GameObject.Find("ParentForDestroy").transform;
		startScale = arrow.transform.localScale;
		isFlying = false;
		FightUpdateLoop.Instance().SubscribeToUpdateLoop(this);
	}
	
	public void MoveArrow(Vector2 direction,float distance)
	{
		//get angle, normalise, reverse angle multiply by distance. move arrow. Rotate?
		if (distance < 0.0f)
		{
			distance = 1.0f;
		}
		if (distance > maxDistance)
		{
			distance = maxDistance;
		}
		float newDistance = (((Mathf.Max(distance,minDistance)+0.1f) / (maxDistance-minDistance)) /2) +0.5f;
		//Debug.Log(newDistance + " with distance: " +distance );
		Vector2 newPosition = (direction.normalized * (maxDistance * newDistance)) + centralPosition;
		arrow.transform.localScale = startScale * newDistance;
		
		if (newPosition.x < centralPosition.x)
		{
			newPosition.x = centralPosition.x;
		}
		
		arrow.transform.position = newPosition;
		
		
		
		
		float newAngle =  -Vector2.Angle(direction.normalized,Vector2.up);
		//Debug.Log(newAngle);
		if (newAngle > 0)
		{
			newAngle = 0;
		} else if (newAngle< -180)
		{
			newAngle = -180;
		}

    	arrow.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,newAngle);
	}

	public void MoveArrow(Vector2 direction)
	{
		//get angle, normalise, reverse angle multiply by distance. move arrow. Rotate?
		
		
		Vector2 newPosition = (direction.normalized * maxDistance) + centralPosition;
		
		if (newPosition.x < centralPosition.x)
		{
			newPosition.x = centralPosition.x;
		}
		
		arrow.transform.position = newPosition;
		
		
		
		
		float newAngle =  -Vector2.Angle(direction.normalized,Vector2.up);
		//Debug.Log(newAngle);
		if (newAngle > 0)
		{
			newAngle = 0;
		} else if (newAngle< -180)
		{
			newAngle = -180;
		}

    	arrow.GetComponent<RectTransform>().rotation = Quaternion.Euler(0,0,newAngle);
	}
	void ResetArrow()
	{
		arrow.transform.position = startPosition;
		arrow.transform.rotation = startRotation;
	}
	public void ActivateArrow()
	{
		isShowing = true;
		arrow.gameObject.SetActive(true);
	}

	public void DeactivateArrow()
	{
		if (isFlying)
		{
			return;
		}
		isShowing = false;
		arrow.gameObject.SetActive(false);
		ResetArrow();
	}
	public void FlyArrowInDirection(Vector2 direction)
	{
		//Debug.Log(direction);
		isFlying = true;
		flyingDirection = direction.normalized;
		Invoke("StopFlying",0.4f);
		InvokeRepeating("FlyArrow",Time.fixedDeltaTime,Time.fixedDeltaTime);
	}
	void FlyArrow()
	{
		arrow.transform.position += new Vector3(flyingDirection.x,flyingDirection.y,0.0f)* flyingSpeed;
	}

	void StopFlying()
	{
		CancelInvoke("FlyArrow");
		isFlying = false;
		DeactivateArrow();
	}

    public bool TimeStep(float deltaTime)
    {
        if (TurnOrder.Instance().GetCurrentTurn() == TurnOrder.Turn.Player)
		{
			ActivateArrow();
		} else {
			DeactivateArrow();
		}
		return false;
    }
}
