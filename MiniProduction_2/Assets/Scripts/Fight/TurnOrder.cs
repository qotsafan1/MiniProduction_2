﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOrder : Manager<TurnOrder>,ITimeAction {
	public enum Turn{Player,Enemy,NoOne,TurnOver,AnimationCoolDown};
	[SerializeField]
	float playerTurnTime;
	[SerializeField]
	float enemyTurnTime;
	int playerToken;
	List<int> turns;

	int[] originalTurns;
	int currentTurnNumber;
	float currentTime;
	public Turn currentTurn;

	void Awake()
	{
		currentTurnNumber = -1;
		currentTurn = Turn.NoOne;
		CalculateEuclidean(FightController.Instance().GetPlayer().agility,FightController.Instance().GetEnemy().agility);
		originalTurns = turns.ToArray();
	}
	void Start()
	{
		FightUpdateLoop.Instance().SubscribeToUpdateLoop(this);
		transform.parent = GameObject.Find("ParentForDestroy").transform;
	}
	
	public void MoveNextEnemyTurnXSpacesBack(int spaces)
	{
		//Find next enemy turn. Move the next five turns one space. Insert the original turn.
		//Connect it to ui somehow
		CheckIfThereIsLessThanSixTurnsInTurnOrder();
		int nextEnemyturn = FindNextEnemyTurn();
		for (int i = 0; i < spaces; i++)
		{
			turns[nextEnemyturn+i] = turns[nextEnemyturn+i+1];
		}
		turns[nextEnemyturn +spaces] = 1;
	}

	void CheckIfThereIsLessThanSixTurnsInTurnOrder()
	{
		if (currentTurnNumber < turns.Count -6)
		{
			for (int i = 0; i < originalTurns.Length; i++)
			{
				turns.Add(originalTurns[i]);
			}
		}
	}

	int FindNextEnemyTurn()
	{
		CheckIfThereIsLessThanSixTurnsInTurnOrder();
		int tempCurrentTurnNumber = currentTurnNumber;
		for (int i = 0; i < turns.Count; i++)
		{
			tempCurrentTurnNumber++;
			if (tempCurrentTurnNumber == turns.Count)
			{
				tempCurrentTurnNumber = 0;
			}
			if (turns[tempCurrentTurnNumber] == 1)
			{
				return tempCurrentTurnNumber;
			}
		}
		return -1;
	}
	void IncrementCurrentTurn()
	{
		CheckIfThereIsLessThanSixTurnsInTurnOrder();
		currentTurnNumber++;
	}

    void CalculateEuclidean(int playerAgility, int enemyAgility)
	{
		//Debug.Log("Player: " + playerAgility + " Enemy: " + enemyAgility);
		int pulses = Mathf.Max(playerAgility,enemyAgility);
		int steps = playerAgility+enemyAgility;
		if (pulses == playerAgility)
		{
			playerToken = 1;
		} else 
		{
			playerToken = 0;
		}

    	int bucket = 0; //out variable to add pulses together for each step
		turns = new List<int>();

    	//fill array with rhythm
		for( int i=0 ; i < steps ; i++)
		{ 
			bucket += pulses; 
			if(bucket >= steps) 
			{
				bucket -= steps;
				if (playerToken == 0)
				{
					turns.Add(1); //'1' indicates a pulse on this beat
				} else 
				{
					turns.Add(0);
				}

			} else 
			{
				if (playerToken == 0)
				{
					turns.Add(0); //'1' indicates a pulse on this beat
				} else 
				{
					turns.Add(1);
				}
			}
		}
		if (turns.Count < 6)
		{
			int number =  turns.Count;
			for (int i = 0; i < number; i++)
			{
				turns.Add(turns[i]);
			}
		}
		if (turns.Count < 6)
		{
			int number =  turns.Count;
			for (int i = 0; i < number; i++)
			{
				turns.Add(turns[i]);
			}
		}
	}

	public void StartTurnSystem()
	{
		IncrementCurrentTurn();
		currentTurn = (Turn)turns[currentTurnNumber];
		currentTime = 0.0f;
	}
	public void PlayerTurnOver()
	{
		NextAdversaryTurn();
		//currentTurn = Turn.Enemy;
		//Debug.Log("Enemy turn");

	}
	public void PlayerAttacked()
	{
		currentTurn = Turn.TurnOver;
	}
	void EnemyTurnOver()
	{
		NextAdversaryTurn();
		//currentTurn = Turn.Player;
		//Debug.Log("Player turn");
	}
	void NextAdversaryTurn()
	{
		currentTime = 0;
		if (currentTurn == Turn.TurnOver)
		{
			if (turns[currentTurnNumber+1] == turns[currentTurnNumber])
			{
				AnimationState state = FightAnimationController.Instance().animationToggler.State;
				//Debug.Log(state);
				if (state == AnimationState.punchchest ||state == AnimationState.punchgroin ||state == AnimationState.punchhead)
				{
					currentTurn = Turn.AnimationCoolDown;
					return;
				}	
			}
		}
		/*if (FightAnimationController.Instance().IsPunching() || FightAnimationController.Instance().IsGettingHit() || BadGuyMotionController.Instance().IsPunching() || BadGuyMotionController.Instance().IsGettingHit())
		{
			currentTurn = Turn.AnimationCoolDown;
		} else */ {
			IncrementCurrentTurn();
			currentTurn = (Turn)turns[currentTurnNumber];
		}
		
	}
	public Turn GetCurrentTurn()
	{
		return currentTurn;
	}
	public int GetCurrentTurnNumber() {
		return currentTurnNumber;
	}
	public int[] GetNextXTurns(int numberOfTurns)
	{
		CheckIfThereIsLessThanSixTurnsInTurnOrder();
		int tempCurrentRound = currentTurnNumber;
		if (tempCurrentRound == -1)
		{
			tempCurrentRound = 0;
		}
		int[] tempTurns = new int[numberOfTurns];

		for (int i = 0; i < numberOfTurns; i++)
		{
			if (tempCurrentRound > turns.Count)
			{
				tempCurrentRound = 0;
			}
			tempTurns[i] = turns[tempCurrentRound];
			tempCurrentRound++;
		}
		return tempTurns;
	}
    public bool TimeStep(float deltaTime)
    {
		currentTime += deltaTime;
		if (currentTurn == Turn.Player)
		{
			if (currentTime > playerTurnTime)
			{
				//Players turn is over
				FightAnimationController.Instance().PlayMiss();
				PlayerTurnOver();
			}
		} else if (currentTurn == Turn.TurnOver)
		{
			if (currentTime > playerTurnTime)
			{
				//Players turn is over
				PlayerTurnOver();
			}
		} else if ( currentTurn == Turn.Enemy)
		{
			if (currentTime > enemyTurnTime)
			{
				//Enemy turn is over
				EnemyTurnOver();
			}
		} else if (currentTurn == Turn.NoOne)
		{
			//If something happens here when its noones turn
			currentTime = 0;

		} else if (currentTurn == Turn.AnimationCoolDown)
		{
			
			//Debug.Log("Animation CoolDown: " + FightAnimationController.Instance().IsPunching());
			AnimationState state = FightAnimationController.Instance().animationToggler.State;
			if (state != AnimationState.punchchest &&state != AnimationState.punchgroin &&state != AnimationState.punchhead || currentTime > 0.5f)
			{
				IncrementCurrentTurn();
				currentTurn = (Turn)turns[currentTurnNumber];
				currentTime = 0;
			}
		}
        return false;
    }

	public void SetTurnToNoOne()
	{
		currentTurn = Turn.NoOne;
	}

	public float GetCurrentTime()
	{
		return currentTime;
	}
	public float GetTurnTime()
	{
		switch (currentTurn)
		{
			case Turn.Player:
			return playerTurnTime;
			case Turn.Enemy:
			return enemyTurnTime;			
			case Turn.NoOne:
			return 0.0f;
		}
		return 0.0f;
	}
}
