﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : Manager<DamageController> {
	float damageVariance = 0.1f;
	void Start()
	{
		transform.parent = GameObject.Find("ParentForDestroy").transform;
	}
	public void GiveDamagerToEnemy(float multiplier)
	{
		PlayDamageSound(FightController.Instance().GetEnemy().isMale);
		//MIKKEL HitIndicator her for dmg på enemy
		EnemyScriptableObject enemy = FightController.Instance().GetEnemy();
		SleeveScriptableObject player = FightController.Instance().GetPlayer();
		float damage = player.attack * multiplier * (1.0f- (enemy.defense/100.0f));
		enemy.currentHealth -= (int)Mathf.Ceil(damage * (1+Random.Range(-damageVariance,damageVariance)));
	}
	public void GiveDamageToPlayer()
	{
		PlayDamageSound(FightController.Instance().GetPlayer().isMale);
		EnemyScriptableObject enemy = FightController.Instance().GetEnemy();
		SleeveScriptableObject player = FightController.Instance().GetPlayer();
		float damage = enemy.attack * (1.0f- (player.defense/100.0f));
		player.currentHealth -= (int)Mathf.Ceil(damage * (1+Random.Range(-damageVariance,damageVariance)));
        particleManager.Instance().PlayerHeadHit();
	}

	void PlayDamageSound(bool isMale)
	{
        AkSoundEngine.PostEvent("play_punch_hit",gameObject);
		string soundToPlay = "";
		if( isMale )
			soundToPlay = "play_male_grunt_1";
		else
			soundToPlay = "play_female_grunt_2";
		AkSoundEngine.PostEvent(soundToPlay,gameObject);
	}
}
