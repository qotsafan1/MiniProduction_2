﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zipper : MonoBehaviour {

    [SerializeField]
    GameObject zipper;
    public bool hasTurned;
    public bool isFinished;
	// Use this for initialization
	void Start () {
        hasTurned = false;
        isFinished = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {   
        if (col.gameObject.name == "Turn")
        {
            hasTurned = true;
        }

        if (col.gameObject.name == "Finish")
        {
            isFinished = true;
            UnzipController.Instance().ShowBody();
        }
    }
}
