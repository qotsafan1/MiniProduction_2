﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterFightHealer : MonoBehaviour {

	[SerializeField]
	SleeveScriptableObject[] roster;

	void Awake()
	{
		for (int i = 0; i < roster.Length; i++)
		{
			

                if (roster[i].currentHealth < 1)
                {
                    roster[i].isActive = false;
                    
                }

                if (roster[i].isReady)
				{
					roster[i].isReady = false;
                } else if (roster[i].isActive)
                {
                    HealFighter(roster[i]);
                }

               
			

		}
	}

	void HealFighter(SleeveScriptableObject sleeve)
	{
		sleeve.currentHealth += (int)(sleeve.health * 0.1f);
		if (sleeve.currentHealth > sleeve.health)
		{
			sleeve.currentHealth = sleeve.health;
		}
	}
}
