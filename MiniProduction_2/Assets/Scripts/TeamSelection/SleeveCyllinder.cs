﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SleeveCyllinder : MonoBehaviour {

    public float angle = 0;
    public GameObject[] cyllinders;
    public GameObject[] innerCyllinders;
    public GameObject[] sleeveContainer;
    public GameObject[] pods;
    public int index = 0;
    public int prevIndex = 1;
    public SleeveScriptableObject[] SOSleeve;
    public int rosterSize;
    [SerializeField]
    public int maxRosterSize = 1;
    float endpos = 0;
    public Text sleeveName;
    public GameObject camTransform;

    public float topCyllinderMaxPos = 2;
    public float topCyllinderMinPos = 1;

    public Light spotLight;

    public float BottomCyllinderMinPos = 0;
    public float BottomCyllinderMaxPos = 3;
    float tresholdValue;
    ModalPanel confirmWindow;
    string confirmText;

    RuntimeAnimatorController StasisAnimController;

    public ParticleSystem blood;


    public void Start()
    {
        spotLight.enabled = false;
        StasisAnimController = Resources.Load("AnimControllers/StasisController") as RuntimeAnimatorController;
        AkSoundEngine.PostEvent("play_display_ambience", gameObject);
        CheckIfYouAreNew();
        tresholdValue = BottomCyllinderMaxPos - 0.5f;
        SelectionSceneUIManager.Instance().DeSelectFighter.onClick.AddListener(SelectFighter);
        SelectionSceneUIManager.Instance().SelectFighter.onClick.AddListener(SelectFighter);
        SelectionSceneUIManager.Instance().destroySleeve.onClick.AddListener(ShowConfirmationWindow);
        SelectionSceneUIManager.Instance().goLeft.onClick.AddListener(rotateLeft);
        SelectionSceneUIManager.Instance().goRight.onClick.AddListener(rotateRight);
        SelectionSceneUIManager.Instance().fight.onClick.AddListener(GoFight);

        for (int i = 0; i < 10; i++)
        {
            //SOSleeve[i].Randomize(100);
            
            sleeveContainer[i] = Instantiate(SOSleeve[i].sleeveModelPrefab);
            sleeveContainer[i].transform.position = new Vector3(pods[i].transform.position.x, 1.00f, pods[i].transform.position.z);
            sleeveContainer[i].transform.rotation = pods[i].transform.rotation;
            sleeveContainer[i].transform.Rotate(0, 180, 0);
            sleeveContainer[i].transform.parent = pods[i].transform;
            

            var Temp = sleeveContainer[i].transform.localPosition;
            Temp.z -= 0.45F;
            sleeveContainer[i].transform.localPosition = Temp;

            var anim = sleeveContainer[i].GetComponent<Animator>();
            anim.runtimeAnimatorController = StasisAnimController;
            anim.Play("stasis");


            sleeveContainer[i].GetComponent<SleeveGameObjectScript>().sleeve = SOSleeve[i];
        }



        SetSliders();
        CheckIfFighterIsSelected();
        DisableUI(index);
        GetComponent<EnemyGenerator>().GenerateEnemy();
    }

    private void Update()
    {


        camTransform.transform.rotation = Quaternion.Lerp(camTransform.transform.rotation, Quaternion.AngleAxis(-angle, Vector3.up), Time.deltaTime * 3);



        if (cyllinders[prevIndex].transform.position.y > -tresholdValue*4)
        {
            innerCyllinders[prevIndex].transform.position = Vector3.Lerp(innerCyllinders[prevIndex].transform.position, new Vector3(innerCyllinders[prevIndex].transform.position.x, topCyllinderMinPos, innerCyllinders[prevIndex].transform.position.z), Time.deltaTime * 3);
            cyllinders[prevIndex].transform.position = Vector3.Lerp(cyllinders[prevIndex].transform.position, new Vector3(cyllinders[prevIndex].transform.position.x, BottomCyllinderMaxPos, cyllinders[prevIndex].transform.position.z), Time.deltaTime * 3);
        }

        if (cyllinders[index].transform.position.y > -tresholdValue*4)
        
        {
            cyllinders[index].transform.position = Vector3.Lerp(cyllinders[index].transform.position, new Vector3(cyllinders[index].transform.position.x, endpos, cyllinders[index].transform.position.z), Time.deltaTime * 3);
            innerCyllinders[index].transform.position = Vector3.Lerp(innerCyllinders[index].transform.position, new Vector3(innerCyllinders[index].transform.position.x, topCyllinderMaxPos, innerCyllinders[index].transform.position.z), Time.deltaTime * 3);
        }

    }



    public void SelectFighter() {
        if (SOSleeve[index].isActive && rosterSize < maxRosterSize && !SOSleeve[index].isReady)
        {
            SOSleeve[index].isReady = true;
            //sleeveContainer[index].GetComponent<Renderer>().material.color = Color.red;
            spotLight.enabled = true;
            AkSoundEngine.PostEvent("play_click", gameObject);
            rosterSize += 1;
        } else if (SOSleeve[index].isReady)
        {
            SOSleeve[index].isReady = false;
            //sleeveContainer[index].GetComponent<Renderer>().material.color = Color.black;
            sleeveContainer[index].GetComponentsInChildren<Renderer>()[0].material.color = Color.white;
            AkSoundEngine.PostEvent("play_click_back", gameObject);
            rosterSize -= 1;
        }
        else if (rosterSize == maxRosterSize)
        {
            Debug.Log("Your team is full");

        }
        CheckIfFighterIsSelected();
        DisableUI(index);
    }

    public void GoFight()
    {

        if (rosterSize > 0)
        {
            AkSoundEngine.PostEvent("stop_all", gameObject);
            AkSoundEngine.PostEvent("play_click", gameObject);
            Debug.Log("fight");
            SceneController.Instance().LoadFightScene();
        }
        else {

            Debug.Log("Pick a team");
            // SceneController.Instance().LoadFightScene();
        }
    }

    public void rotateRight()
    {
        blood.Stop();
        endpos = BottomCyllinderMinPos;
        prevIndex = index;
        angle += 36;
        AkSoundEngine.PostEvent("play_Rotunda", gameObject);
        if (index == 9)
        {
            index = 0;
        }
        else
        {
            index += 1;
        }


        SetSliders();
    }

    public void rotateLeft()
    {
        blood.Stop();
        endpos = BottomCyllinderMinPos;
        prevIndex = index;
        angle -= 36;
        AkSoundEngine.PostEvent("play_Rotunda", gameObject);
        if (index == 0)
        {
            index = 9;
        }
        else
        {
            index -= 1;
        }
        SetSliders();

    }

    public void DestroySleeve()
    {
       
        if (SOSleeve[index].isActive)
        {
            pods[index].transform.parent = cyllinders[index].transform;
            endpos = -BottomCyllinderMaxPos*2.5f;
            SOSleeve[index].isActive = false;
            if (SOSleeve[index].isReady)
            {
                SOSleeve[index].isReady = false;
                rosterSize -= 1;
            }

            blood.Play();
            
            AkSoundEngine.PostEvent("Play_Grinder", gameObject);
        }
        DisableUI(index);
        spotLight.enabled = true;
        spotLight.color = Color.red;
        for (int i = 0; i < SOSleeve.Length; i++)
        {
            if (SOSleeve[i].isActive)
            {
                return;
            }

        }
        SceneController.Instance().LoadGameOverScene();
      
    }

    int sleeveCount;
    void ShowConfirmationWindow()
    {
        confirmWindow = ModalPanel.Instance();
        sleeveCount = 0;
        for (int i = 0; i < SOSleeve.Length; i++)
        {
            if (SOSleeve[i].isActive) sleeveCount++;
        }
        if( sleeveCount == 1 ) confirmText = "confirmOver";
        else confirmText = "confirmDiscard";
        
        confirmWindow.Choice("", DestroySleeve, CloseConfirmWindow);
        confirmWindow.GetComponentInChildren<LocalizedText>().key = confirmText;
        confirmWindow.GetComponentInChildren<LocalizedText>().UpdateText();

        AkSoundEngine.PostEvent("play_click",gameObject);
        AkSoundEngine.PostEvent("play_ui_open",gameObject);
    }

    void CloseConfirmWindow()
    {
        confirmWindow.gameObject.SetActive(false);
        AkSoundEngine.PostEvent("play_click_back",gameObject);
        AkSoundEngine.PostEvent("play_ui_close", gameObject);
    }


    public void SetSliders()
    {


        SelectionSceneUIManager.Instance().agilitySlider.value = SOSleeve[index].agility;
        SelectionSceneUIManager.Instance().attackSlider.value = SOSleeve[index].attack;
        SelectionSceneUIManager.Instance().healthSlider.value = SOSleeve[index].health;
        SelectionSceneUIManager.Instance().defenseSlider.value = SOSleeve[index].defense;
        SelectionSceneUIManager.Instance().sleeveName.text = SOSleeve[index].sleeveName;
        DisableUI(index);
        CheckIfFighterIsSelected();
    }

    public void DisableUI(int i)
    {
        CheckIfFighterIsSelected();
      

        if (!SOSleeve[i].isActive)
        {
            SelectionSceneUIManager.Instance().gAgilitySlider.SetActive(false);
            SelectionSceneUIManager.Instance().gAttackSlider.SetActive(false);
            SelectionSceneUIManager.Instance().gHealthSlider.SetActive(false);
            SelectionSceneUIManager.Instance().gDefenseSlider.SetActive(false);
            SelectionSceneUIManager.Instance().sleeveName.enabled = false;

            SelectionSceneUIManager.Instance().gDestroySleeve.SetActive(false);
            SelectionSceneUIManager.Instance().gSelectFighter.SetActive(false);
           SelectionSceneUIManager.Instance().gDeSelectFighter.SetActive(false);
            SelectionSceneUIManager.Instance().upcomingFighter.SetActive(false);


        }
        else
        {

            SelectionSceneUIManager.Instance().gAgilitySlider.SetActive(true);
            SelectionSceneUIManager.Instance().gAttackSlider.SetActive(true);
            SelectionSceneUIManager.Instance().gHealthSlider.SetActive(true);
            SelectionSceneUIManager.Instance().gDefenseSlider.SetActive(true);
            SelectionSceneUIManager.Instance().sleeveName.enabled = true;

            SelectionSceneUIManager.Instance().gDestroySleeve.SetActive(true);
            SelectionSceneUIManager.Instance().upcomingFighter.SetActive(true);
            // SelectionSceneUIManager.Instance().gSelectFighter.SetActive(true);
            // SelectionSceneUIManager.Instance().gDeSelectFighter.SetActive(true);

        }

        if (rosterSize == 0) {
           
            SelectionSceneUIManager.Instance().gFight.SetActive(false);
        }
        else { 
            SelectionSceneUIManager.Instance().gFight.SetActive(true);
      }
    }


    public void CheckIfYouAreNew()
    {
        for (int i = 0; i < SOSleeve.Length; i++)
        {
            if (SOSleeve[i].isActive)
            {
                return;
            }
        }
        SOSleeve[0].Randomize(100);
        GetComponent<EnemyGenerator>().Enemy.isFirstTime = true;
        SOSleeve[0].isActive = true;

    }
    
    public void CheckIfFighterIsSelected()
    {
        if (SOSleeve[index].isReady)
        {
            SelectionSceneUIManager.Instance().gDeSelectFighter.SetActive(true);
            SelectionSceneUIManager.Instance().gSelectFighter.SetActive(false);
            spotLight.enabled = true;
            spotLight.color = Color.green;
        }
        else if(SOSleeve[index].isActive)
        {
            SelectionSceneUIManager.Instance().gDeSelectFighter.SetActive(false);
            SelectionSceneUIManager.Instance().gSelectFighter.SetActive(true);
            spotLight.enabled = false;
        }
        else
        {
            SelectionSceneUIManager.Instance().gDeSelectFighter.SetActive(false);
            SelectionSceneUIManager.Instance().gSelectFighter.SetActive(false);
            spotLight.enabled = false;
        }


    }

}
