﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdminPanel : MonoBehaviour {
	static AdminPanel adminPanel;
	[SerializeField]
	Text debugInfoText;
	public static AdminPanel Instance () 
	{
		if (!adminPanel) {
			adminPanel = FindObjectOfType(typeof (AdminPanel)) as AdminPanel;
			if (!adminPanel)
			{
				//Debug.LogError ("There needs to be one active ModalPanel script on a GameObject in your scene.");
				GameObject temp = (GameObject)Instantiate(Resources.Load("Admin/adminPanel"),Vector3.zero,Quaternion.identity);
				adminPanel = temp.GetComponent<AdminPanel>();
			}
		}
		
		return adminPanel;
	}

	public void DisplayDebug(string debugInfo)
	{
		debugInfoText.text += "\n"+debugInfo;
	}
	
}
