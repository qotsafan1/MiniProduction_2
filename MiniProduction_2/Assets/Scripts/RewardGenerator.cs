﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class RewardGenerator : MonoBehaviour {


    public SleeveScriptableObject[] SOSleeve;


    public SleeveScriptableObject Generate( int powerLevel)
    {
        for (int i = 0; i < SOSleeve.Length; i++)
        {
            if (!SOSleeve[i].isActive)
            {
                SOSleeve[i].Randomize(powerLevel);
                SOSleeve[i].isActive = true;
                return SOSleeve[i];


            }
            
        }
        return null;

    }

}
