﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleeveGameObjectScript : MonoBehaviour {

    public SleeveScriptableObject sleeve;

   


    [SerializeField]
    public ParticleSystem particleHead;

    [SerializeField]
    public ParticleSystem particleTorso;

    [SerializeField]
    public ParticleSystem particleLower;


    // Use this for initialization
    void Start () {


     
        if (sleeve == null || !sleeve.isActive)
        {
            gameObject.SetActive(false);

        }

        
	}
	

}
