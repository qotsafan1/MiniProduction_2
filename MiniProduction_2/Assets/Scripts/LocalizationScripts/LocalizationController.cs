﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationController : Manager<LocalizationController> {

	private Dictionary<string, string> localizedText;
	private string missingTextString = "Localized text not found";

    List<LocalizedText> activeTextObjects;
    void Awake()
    {
        //PlayerPrefs.SetString("preferedLanguageFile","englishLanguage");

        activeTextObjects = new List<LocalizedText>();
        LoadPreferedLanguage();
    }
    public void SubscribeAsActiveTextObject(LocalizedText newObject)
    {
        activeTextObjects.Add(newObject);
    }
    public void UnSubscribeAsActiveTextObject(LocalizedText oldObject)
    {
        activeTextObjects.Remove(oldObject);
    }

    void UpdateLanguageInAllActiveTextObjects()
    {
        if (activeTextObjects.Count == 0){return;}
        foreach(LocalizedText text in activeTextObjects)
        {
            text.UpdateText();
        }
    }

    public void LoadPreferedLanguage()
    {
        if (PlayerPrefs.HasKey("preferedLanguageFile"))
        {
            LoadLocalizedText(PlayerPrefs.GetString("preferedLanguageFile"));
        }
        else if (!SceneController.Instance().IsSplashScreen())
        {
            LoadLocalizedText("englishLanguage");
        }
    }

	public void LoadLocalizedText(string fileName)
    {
        PlayerPrefs.SetString("preferedLanguageFile",fileName);
        localizedText = new Dictionary<string, string> ();
           
        //AdminPanel.Instance().DisplayDebug("Is running");
        //Resources Variant
        //TextAsset file = Resources.Load("LocalizationData/englishLanguage") as TextAsset;
        Debug.Log(fileName);
        TextAsset file = Resources.Load("LocalizationData/" + fileName) as TextAsset;
        
        string content = file.ToString();
        LocalizationData loadedData = JsonUtility.FromJson<LocalizationData> (content);
        
        //LocalizationData loadedData = JsonUtility.FromJson<LocalizationData> (dataAsJson);

        for (int i = 0; i < loadedData.items.Length; i++) 
        {
            localizedText.Add (loadedData.items [i].key, loadedData.items [i].value);   
        }

        Debug.Log ("Data loaded, dictionary contains: " + localizedText.Count + " entries");
        //AdminPanel.Instance().DisplayDebug("Data loaded, dictionary contains: " + localizedText.Count + " entries");
        UpdateLanguageInAllActiveTextObjects();
            
	}
	public string GetLocalizedValue(string key)
    {
        string result = missingTextString;
        if (localizedText.ContainsKey (key)) 
        {
            result = localizedText [key];
        }

        return result;

    }
    
}
