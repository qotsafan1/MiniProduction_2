﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class LanguageSelection : MonoBehaviour, IPointerClickHandler {
	public string filename;
	public UnityEvent chosenLanguage;
	public void OnPointerClick(PointerEventData eventData)
    {
        LocalizationController.Instance().LoadLocalizedText(filename);
		chosenLanguage.Invoke();
    }

}
