﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoPlayer : MonoBehaviour {

	[SerializeField]
	Sprite[] logoTextures;
	[SerializeField]
	Image logoimage;
	
	//float currentTime;
	bool isRunning = false;
	int frameCount;
	// Use this for initialization
	void Start () {
		//currentTime = 0;
		frameCount = 0;
		isRunning = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (isRunning)
		{
			frameCount++;
			//currentTime += Time.fixedDeltaTime;
			PlayLogo();
			
		}
	}

	void PlayLogo()
	{
		if( frameCount == 48 ) 
			logoimage.sprite = logoTextures[1];
		else if ( frameCount == 96 )
			logoimage.sprite = logoTextures[0];
		else if ( frameCount == 144 )
			logoimage.sprite = logoTextures[1];
		else if ( frameCount == 192 )
			logoimage.sprite = logoTextures[0];
		else if ( frameCount == 240 )
			logoimage.sprite = logoTextures[2];
		else if ( frameCount == 241 )
			logoimage.sprite = logoTextures[3];
		else if ( frameCount == 248 )
			logoimage.sprite = logoTextures[2];
		else if ( frameCount == 249 )
			logoimage.sprite = logoTextures[4];
		else if ( frameCount == 255 )
			logoimage.sprite = logoTextures[2];
		else if ( frameCount == 256 )
			logoimage.sprite = logoTextures[3];
		else if ( frameCount == 280 )
			logoimage.sprite = logoTextures[0];
	}
}
