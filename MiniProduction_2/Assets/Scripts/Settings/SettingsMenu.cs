﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {

	bool settingsVisible = false;

    public GameObject settingsMenu;

    [SerializeField]
    Image danish;
    [SerializeField]
    Image english;

    void Start()
    {
        if (PlayerPrefs.HasKey("preferedLanguageFile"))
        {
            if (PlayerPrefs.GetString("preferedLanguageFile") == "danishLanguage")
            {
                ChangeLanguage("dk");
                return;
            }
        }
        ChangeLanguage("gb");
    }

	public void ChangeSettingsMenu()
    {
        if (settingsVisible)
        {
            settingsVisible = false;
            settingsMenu.SetActive(false);
        }
        else
        {

            settingsVisible = true;
            settingsMenu.SetActive(true);
        }
	}

    public void ChangeLanguage(string language)
    {
        if (language == "gb")
        {
            danish.color = Color.grey;
            english.color = Color.white;
        } else 
        {
            danish.color = Color.white;
            english.color = Color.grey;

        }
        AkSoundEngine.PostEvent( "play_click", gameObject);
    }
}
