﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsScroll : MonoBehaviour {
	[SerializeField]
	GameObject creditsText;
	public float scrollingSpeed;
	public void StartScroll()
	{
		creditsText.transform.parent.gameObject.SetActive(true);
		Vector3 newStartingPosition = Vector3.zero;
		newStartingPosition.x += Screen.width/2;
		newStartingPosition.y -= (Screen.height/2) -100;
		creditsText.transform.position = newStartingPosition;
		InvokeRepeating("Scrolling",Time.fixedDeltaTime,Time.fixedDeltaTime);
	}

	void Scrolling()
	{
		creditsText.transform.position += Vector3.up * scrollingSpeed;
	}

	public void StopScrolling()
	{
		CancelInvoke("Scrolling");
		creditsText.transform.parent.gameObject.SetActive(false);
		AkSoundEngine.PostEvent("play_click_back", gameObject);
	}
}
