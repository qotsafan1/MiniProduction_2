﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlay : MonoBehaviour {

	void Start()
	{
		if (PlayerPrefs.HasKey("ShownTutorial"))
		{
			CloseImage();
		}
	}
	public void CloseImage()
	{
		gameObject.SetActive(false);
		PlayerPrefs.SetInt("ShownTutorial",1);
		FightController.Instance().isRunning = true;
	}
}
