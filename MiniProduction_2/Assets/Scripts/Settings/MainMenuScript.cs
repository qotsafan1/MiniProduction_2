﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour {

    [SerializeField]
    GameObject settingsWindow;

    [SerializeField]
    GameObject buttonsGroup;
    [SerializeField]
    GameObject background;
    [SerializeField]
    Camera mainMenuCamera;
    [SerializeField]
    GameObject backToMenuButton;
    [SerializeField]
    GameObject logo;

    void Start()
    {
        AkSoundEngine.PostEvent("play_menu_ambience", gameObject);
    }

    private ModalPanel confirmWindow;
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape) )
        {
            if (buttonsGroup.activeSelf)
            {
                OnQuitClick();
            } else 
            {
                ShowMainMenu();
            }
        }
	}

    public void OnPlayClick()
    {
        AkSoundEngine.PostEvent("play_click", gameObject);
        AkSoundEngine.PostEvent( "stop_ambience", gameObject);
        AkSoundEngine.SetRTPCValue("menu_filter", 1);
        settingsWindow.SetActive(false);
        if (SceneController.Instance().GetNumberOfOpenScenes() == 1)
        {
            SceneController.Instance().StartGame();
        }
        backToMenuButton.SetActive(true);
        CloseMainMenu();
    }

    public void OnQuitClick()
    {
		confirmWindow = ModalPanel.Instance();
        confirmWindow.Choice("",QuitGame,CloseConfirmWindow);
        confirmWindow.GetComponentInChildren<LocalizedText>().key = "confirmMenu";
        confirmWindow.GetComponentInChildren<LocalizedText>().UpdateText();
        settingsWindow.SetActive(false);
        AkSoundEngine.PostEvent("play_click", gameObject);
        AkSoundEngine.PostEvent("play_ui_open",gameObject);
    }
    public void OnSettingsClick()
    {
        settingsWindow.SetActive(!settingsWindow.activeSelf);

        if(settingsWindow.activeSelf)
            logo.SetActive(false);
        else
            logo.SetActive(true);

        AkSoundEngine.PostEvent("play_click", gameObject);
        AkSoundEngine.PostEvent("play_ui_open",gameObject);
    }
    public void OnCreditsClick()
    {
        GetComponent<CreditsScroll>().StartScroll();
        settingsWindow.SetActive(false);
        AkSoundEngine.PostEvent("play_click",gameObject);
    }
    public void OnBackClick()
    {
        AkSoundEngine.PostEvent("play_click_back",gameObject);
    }

	public void QuitGame()
	{
		Debug.Log("Quitting");
        Application.Quit();
	}
    void CloseMainMenu()
    {
        buttonsGroup.SetActive(false);
        background.SetActive(false);
        mainMenuCamera.gameObject.SetActive(false);
    }
    public void ShowMainMenu()
    {
        buttonsGroup.SetActive(true);
        background.SetActive(true);
        mainMenuCamera.gameObject.SetActive(true);
        logo.SetActive(true);
        backToMenuButton.SetActive(false);
        AkSoundEngine.PostEvent("play_click_back",gameObject);
        AkSoundEngine.SetRTPCValue("menu_filter", 0);
        AkSoundEngine.PostEvent("play_menu_ambience", gameObject);        
    }
    public void CloseConfirmWindow()
    {
        confirmWindow.gameObject.SetActive(false);
        AkSoundEngine.PostEvent("play_click_back", gameObject);
        AkSoundEngine.PostEvent("play_ui_close", gameObject);
    }
    public bool IsMainMenuOpen()
    {
        return buttonsGroup.activeSelf;
    }
}
