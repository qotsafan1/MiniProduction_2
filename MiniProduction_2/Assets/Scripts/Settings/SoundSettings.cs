﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSettings : MonoBehaviour {

	public Slider musicVolumeSlider;
    public Slider sfxVolumeSlider;
    public Toggle musicToggle;
    public Toggle sfxToggle;

	// Use this for initialization
	void Start () {
		musicVolumeSlider.value = PlayerPrefs.GetInt("MusicVolume", 65) ;
        sfxVolumeSlider.value = PlayerPrefs.GetInt("SFXVolume", 80);
        musicToggle.isOn = (PlayerPrefs.GetInt("MusicToggle", 0) != 1) ;
        sfxToggle.isOn = (PlayerPrefs.GetInt("SFXToggle", 0) != 1);
        Debug.Log(sfxToggle.isOn);
        /*Debug.Log(PlayerPrefs.GetInt("MusicVolume"));
        Debug.Log(PlayerPrefs.GetInt("SFXVolume"));
        Debug.Log(PlayerPrefs.GetInt("MusicToggle"));
        Debug.Log(PlayerPrefs.GetInt("SFXToggle"));*/
	}
	
	public void ChangeMusicValue()
    {
        PlayerPrefs.SetInt("MusicVolume", (int)musicVolumeSlider.value);
        if ((PlayerPrefs.GetInt("MusicToggle")!=1))
        {
           
            AkSoundEngine.SetRTPCValue("music_global", musicVolumeSlider.value);
        }
    }

    public void SwitchMusic()
    {

        if (musicToggle.isOn)
        {
            
            AkSoundEngine.SetRTPCValue("music_global", musicVolumeSlider.value);
            //Debug.Log("Music muted");
            PlayerPrefs.SetInt("MusicToggle",0);
            //Debug.Log("It should be 0: "+PlayerPrefs.GetInt("MusicToggle"));
        }
        else
        {
            AkSoundEngine.SetRTPCValue("music_global", 0);
            //Debug.Log("Music unmuted");
            PlayerPrefs.SetInt("MusicToggle", 1);
            //Debug.Log("It should be 1: "+PlayerPrefs.GetInt("MusicToggle"));
        }
    }

    public void ChangeSFXValue()
    {
        PlayerPrefs.SetInt("SFXVolume", (int)sfxVolumeSlider.value);
        if ((PlayerPrefs.GetInt("SFXToggle") != 1))
        {
            
            AkSoundEngine.SetRTPCValue("sfx_global", sfxVolumeSlider.value);
        }        
    }

    public void SwitchSFX()
    {
        if (sfxToggle.isOn)
        {
            AkSoundEngine.SetRTPCValue("sfx_global", sfxVolumeSlider.value);
            Debug.Log("SFX muted");
            PlayerPrefs.SetInt("SFXToggle", 0);
        }
        else 
        {
            AkSoundEngine.SetRTPCValue("sfx_global", 0);
            Debug.Log("SFX unmuted");
            PlayerPrefs.SetInt("SFXToggle", 1);
        }
    }
}
