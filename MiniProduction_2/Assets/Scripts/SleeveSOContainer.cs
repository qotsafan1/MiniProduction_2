﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
[CreateAssetMenu(fileName = "new container", menuName = "SleeveContainer")]
public class SleeveSOContainer : ScriptableObject {

    public ScriptableObject[] SOSleeve;

}
