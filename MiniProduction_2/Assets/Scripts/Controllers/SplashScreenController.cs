﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreenController : MonoBehaviour {

	[SerializeField]
	Image unityLogo;
	[SerializeField]
	Image dadiuLogo;
	[SerializeField]
	Image gameLogo;
	[SerializeField]
	GameObject dansihFlag;
	[SerializeField]
	GameObject englishFlag;

	float currentTime;
	bool isRunning = false;

	
	void Start()
	{
		currentTime = 0;
        if (PlayerPrefs.HasKey("preferedLanguageFile"))
		{
			LanguageChosen();
		}
	}

	void Update()
	{
		if (isRunning)
		{
			currentTime += Time.deltaTime;
			RunSplashScreens();
		}
	}

	void RunSplashScreens()
	{
		
		switch((int)currentTime)
		{
			case 1:
			dadiuLogo.gameObject.SetActive(true);
			break;
			case 3:
			dadiuLogo.gameObject.SetActive(false);
			unityLogo.gameObject.SetActive(true);
			break;
			case 5:
			gameLogo.gameObject.SetActive(true);
			unityLogo.gameObject.SetActive(false);
			break;
			case 12:
			AkSoundEngine.PostEvent("stop_all", gameObject);
			SceneController.Instance().LoadMainMenu();
			isRunning = false;
			return;

		}
	}

	public void LanguageChosen()
	{
		dansihFlag.SetActive(false);
		englishFlag.SetActive(false);
		isRunning = true;
	}

}
