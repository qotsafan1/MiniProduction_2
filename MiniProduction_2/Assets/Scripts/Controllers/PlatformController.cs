﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : Manager<PlatformController>
{
    public bool MovementAllowed { get; set; }

    bool moveToLeft = false;
    bool moveToRight = false;
    string position;
    string towardsPosition;

    [SerializeField]
    GameObject EndPoint;
    [SerializeField]
    GameObject RightPoint;
    [SerializeField]
    GameObject LeftPoint;
    public GameObject PlatformParent;
    float speed = 1.5f;

    public int numberOfChosenReward;

    public SleeveScriptableObject[] rewardedSleeves = new SleeveScriptableObject[3];

    // Use this for initialization
    void Start ()
    {
        MovementAllowed = false;
        position = "middle";
        towardsPosition = "middle";
    }

    private void MoveToPosition()
    {
        // The step size is equal to speed times frame time.
        float step = speed * Time.deltaTime;

        // Move our position a step closer to the target.
        transform.position = Vector3.MoveTowards(transform.position, EndPoint.transform.position, step);

        if (transform.position == EndPoint.transform.position)
        {
            MovementAllowed = false;

            if (numberOfChosenReward == 1)
            {
                Debug.Log("1");
                //MoveToRight();
                UIRewardController.Instance().SetSliders(rewardedSleeves[1]);
            }
            else if (numberOfChosenReward == 0)
            {
                Debug.Log("0");
                MoveToRight();
                //UIRewardController.Instance().SetSliders(rewardedSleeves[0]);
            }
            else
            {
                Debug.Log("2");
                MoveToLeft();
                //UIRewardController.Instance().SetSliders(rewardedSleeves[2]);                
            }

            UIRewardController.Instance().StatPanel.SetActive(true);
        }        
    }

    public void MoveBetweenRewards()
    {
        // The step size is equal to speed times frame time.
        float step = 7f * Time.deltaTime;

        if ((position == "left" || position == "right") && towardsPosition == "middle")
        {
            transform.position = Vector3.MoveTowards(transform.position, EndPoint.transform.position, step);
            if (transform.position == EndPoint.transform.position)
            {
                position = "middle";
                ResetBool();

                if (rewardedSleeves[1] != null)
                {
                    UIRewardController.Instance().SetSliders(rewardedSleeves[1]);
                }
                else
                {
                    UIRewardController.Instance().SetSlidersEmpty();
                }
            }
        }
        else if (position == "middle" && towardsPosition == "right")
        {
            transform.position = Vector3.MoveTowards(transform.position, RightPoint.transform.position, step);
            if (transform.position == RightPoint.transform.position)
            {
                position = "right";
                ResetBool();

                if (rewardedSleeves[0] != null)
                {
                    UIRewardController.Instance().SetSliders(rewardedSleeves[0]);
                }
                else
                {
                    UIRewardController.Instance().SetSlidersEmpty();
                }
            }
        }
        else if (position == "middle" && towardsPosition == "left")
        {
            transform.position = Vector3.MoveTowards(transform.position, LeftPoint.transform.position, step);
            if (transform.position == LeftPoint.transform.position)
            {
                position = "left";
                ResetBool();

                if (rewardedSleeves[2] != null)
                {
                    UIRewardController.Instance().SetSliders(rewardedSleeves[2]);
                }
                else
                {
                    UIRewardController.Instance().SetSlidersEmpty();
                }
            }
        }
        else
        {
            ResetBool();
        }
    }

    public void MoveToRight()
    {
        if (position == "middle")
        {
            towardsPosition = "right";
            moveToRight = true;
        }
        else if (position == "left")
        {
            towardsPosition = "middle";
            moveToRight = true;
        }
    }

    public void MoveToLeft()
    {
        if (position == "middle")
        {
            towardsPosition = "left";
            moveToLeft = true;
        }
        else if (position == "right")
        {
            towardsPosition = "middle";
            moveToLeft = true;
        }
    }

    public void ResetBool()
    {
        moveToLeft = false;
        moveToRight = false;
    }

    public void ActivatePlatforms()
    {
        MovementAllowed = true;        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Body")
        {
            UnzipController.Instance().amountOfCollectedRewards++;
        }
    }

    // Update is called once per frame
    void Update ()
    {
	    if (MovementAllowed)
        {
            MoveToPosition();
        }

        if (moveToLeft || moveToRight)
        {
            MoveBetweenRewards();
        }
    }
}
