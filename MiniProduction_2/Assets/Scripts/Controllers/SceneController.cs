﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : Manager<SceneController> {

	
	string[] fightScene = new string[] {"FightScene","FightSceneBackground","FightSceneUI"};
	string[] buildScene = new string[] {"BuildLogic","BuildUI" };
	string[] rewardScene = new string[] {"RewardEnv","RewardLogic","RewardUI" };
	string[] loseScene = new string[] {"LoseEnv","LoseUI","RewardEnv" };
	string[] gameoverScene = new string[] {"GameOverScene"};
	string[] menu = new string[] {"Menu"};
	string[] splashScreen = new string[] {"SplashScreen"};

	bool firstLoad;

	public List<string> scenesOpen;
	public List<string> permenantScenes;
	void Awake()
	{
		scenesOpen = new List<string>();
		permenantScenes = new List<string>();
		firstLoad = true;
		AddOpenScenesToList();
	}
	void AddOpenScenesToList()
	{
		for (int i = 0; i < SceneManager.sceneCount; i++)
		{
			scenesOpen.Add(SceneManager.GetSceneAt(i).name);
			if (SceneManager.GetSceneAt(i).name == menu[0])
			{
				permenantScenes.Add(SceneManager.GetSceneAt(i).name);
			}
		}
	}

	public void StartGame()
	{
		if (SceneManager.sceneCount == 1)
		{
			
		}
		LoadBuildTeam();
	}
	public void FightOver(bool isWin)
	{
		if (isWin)
		{
			LoadRewardScene();
		} else {
			LoadLoseScene();
		}
	}

	public bool IsSplashScreen()
	{
		for (int i = 0; i < SceneManager.sceneCount; i++)
		{
			if (SceneManager.GetSceneAt(i).name == "SplashScreen")
			return true;
		}
		return false;
	}

	void MakeScenePermenant(string permenantScene)
	{
		permenantScenes.Add(permenantScene);
	}

	void LoadMultipleScenes(string[] scenesToLoad)
	{
		try{
			for (int i = 0; i < scenesToLoad.Length; i++)
			{
				if (permenantScenes.Count == 0 && firstLoad)
				{
					firstLoad = false;
					scenesOpen.Clear();
					SceneManager.LoadScene(scenesToLoad[i],LoadSceneMode.Single);
				} else {
					SceneManager.LoadScene(scenesToLoad[i],LoadSceneMode.Additive);
				}
				scenesOpen.Add(scenesToLoad[i]);
			}
			firstLoad = false;
		} catch{
			Debug.Log("Not all scenes were loaded");
		}
	}

	void UnloadOpenScenes()
	{
		List<string> deletedScenes = new List<string>();
		for (int i = 0; i < scenesOpen.Count; i++)
		{
			bool delete = true;
			for (int j = 0; j < permenantScenes.Count; j++)
			{
				if (scenesOpen[i] == permenantScenes[j] )
				{
					delete = false;
				}
			}
			if (delete)
			{
				deletedScenes.Add(scenesOpen[i]);
				SceneManager.UnloadSceneAsync(scenesOpen[i]);
			}
		}
		if (deletedScenes.Count != 0)
		{
			for (int i = 0; i < deletedScenes.Count; i++)
			{
				scenesOpen.Remove(deletedScenes[i]);
			}
		}
	}

	public void LoadSplashScreen()
	{
		LoadMultipleScenes(splashScreen);
	}
	public void LoadMainMenu()
	{
		if (permenantScenes.Count == 0)
		{
			UnloadOpenScenes();
			LoadMultipleScenes(menu);
			MakeScenePermenant(menu[0]);
		} else {
			UnloadOpenScenes();
			GameObject.Find("MainMenu").GetComponent<MainMenuScript>().ShowMainMenu();
		}
	}
	public void LoadBuildTeam()
	{
		UnloadOpenScenes();
		LoadMultipleScenes(buildScene);
	}
	public void LoadFightScene()
	{
		UnloadOpenScenes();
		LoadMultipleScenes(fightScene);
	}
	public void LoadRewardScene()
	{
		UnloadOpenScenes();
		LoadMultipleScenes(rewardScene);
	}
	public void LoadLoseScene()
	{
		UnloadOpenScenes();
		LoadMultipleScenes(loseScene);
	}
	public void LoadGameOverScene()
	{
		UnloadOpenScenes();
		LoadMultipleScenes(gameoverScene);
	}
	public int GetNumberOfOpenScenes()
	{
		return SceneManager.sceneCount;
	}
	
}
