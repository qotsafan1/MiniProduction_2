﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnzipController : Manager<UnzipController>
{
    int layerMask;
    GameObject zipper;
    Plane objPlane;
    Vector3 m0;

    public int amountOfRewards;
    public int amountOfCollectedRewards;

    public bool priceFulfilled;
    bool stopZipperSound = false;

    bool zippingFinished;
    [SerializeField]
    EnemyScriptableObject enemy;

    // Use this for initialization
    void Start () {
	    layerMask = LayerMask.GetMask("Zipper");
        zippingFinished = false;
        amountOfRewards = 1;
        amountOfCollectedRewards = 0;
		transform.parent = GameObject.Find("ParentForDestroy").transform;
        AkSoundEngine.PostEvent( "play_crowd_win", gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        moveZipper();

        if (!zippingFinished && amountOfCollectedRewards == amountOfRewards)
        {
            UIRewardController.Instance().GoToBuildScene.SetActive(true);
            UIRewardController.Instance().ChoosePriceImage.SetActive(false);
            priceFulfilled = true;
            PlatformController.Instance().ActivatePlatforms();
            zippingFinished = true;
        }
    }

    public void ShowBody()
    {
        zipper.transform.parent.GetChild(0).gameObject.SetActive(false);
        zipper.transform.parent.GetChild(3).gameObject.SetActive(false);
        zipper.SetActive(false);        
        
        GameObject body = zipper.transform.parent.GetChild(1).gameObject;

        Vector3 thePosition = body.transform.position;
        Quaternion theRotation = body.transform.rotation;
        int rewardNumber = body.GetComponent<RewardHandler>().rewardNumber;
        BoxCollider boxCollider = body.GetComponent<BoxCollider>();        

        int rewardSleevePowerLevel = EnemyController.Instance().GetPowerLevel(enemy.enemyLevel);
        rewardSleevePowerLevel += 40;
        //do randomizations maybe to the number

        SleeveScriptableObject reward = GetComponent<RewardGenerator>().Generate(rewardSleevePowerLevel);
        GameObject newBody = Instantiate(reward.sleeveModelPrefab);
        newBody.transform.position = thePosition;
        newBody.transform.rotation = theRotation;
        newBody.name = "Body";
        newBody.GetComponent<SleeveGameObjectScript>().sleeve = reward;
        newBody.transform.SetParent(PlatformController.Instance().PlatformParent.transform);
        newBody.AddComponent<Rigidbody>();

        Animator animator = newBody.GetComponent<Animator>();
        animator.runtimeAnimatorController = Resources.Load("StandUpController") as RuntimeAnimatorController;
        animator.applyRootMotion = false;        

        //CopyComponent(body.GetComponent<BoxCollider>(), newBody);
        BoxCollider newBoxCollider = newBody.AddComponent<BoxCollider>() as BoxCollider;
        newBoxCollider.center = boxCollider.center;
        newBoxCollider.size = boxCollider.size;
        newBody.SetActive(true);
        PlatformController.Instance().numberOfChosenReward = rewardNumber;
        PlatformController.Instance().rewardedSleeves[rewardNumber] = reward;
    }

    public void GoToBuildScene()
    {
        SceneController.Instance().LoadBuildTeam();
    }

    private void moveZipper()
    {
        if (priceFulfilled)
        {
            return;
        }

        if( stopZipperSound ) 
        {
            AkSoundEngine.PostEvent("stop_zipper_loop", gameObject);
            stopZipperSound = false;
        }

        if (Input.GetMouseButtonDown(0) && zipper == null)
        {
            Ray mouseRay = GenerateMouseRay();
            RaycastHit hit;
            if (Physics.Raycast(mouseRay.origin, mouseRay.direction, out hit, Mathf.Infinity, layerMask))
            {
                zipper = hit.transform.gameObject;

                if (zipper.GetComponent<Zipper>().isFinished)
                {
                    stopZipperSound = true;
                    return;
                }

                objPlane = new Plane(Camera.main.transform.forward * -1, zipper.transform.position);

                // calc mouse offset
                Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                float rayDistance;
                objPlane.Raycast(mRay, out rayDistance);
                m0 = zipper.transform.position - mRay.GetPoint(rayDistance);
                AkSoundEngine.PostEvent("play_zipper_loop", gameObject);
            }
        }
        else if (Input.GetMouseButton(0) && zipper != null)
        {
            if (zipper.GetComponent<Zipper>().isFinished)
            {
                stopZipperSound = true;
                return;
            }

            Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            float rayDistance;

            if (objPlane.Raycast(mRay, out rayDistance))
            {
                Vector3 newPoints = mRay.GetPoint(rayDistance) + m0;

                if (!zipper.GetComponent<Zipper>().hasTurned && newPoints.y < zipper.transform.position.y)
                {
                    zipper.transform.position = new Vector3(zipper.transform.position.x, newPoints.y, newPoints.z);
                }
                else if (zipper.GetComponent<Zipper>().hasTurned && newPoints.x < zipper.transform.position.x)
                {
                    zipper.transform.position = new Vector3(newPoints.x, zipper.transform.position.y, newPoints.z);
                }
            }
        }
        else if (Input.GetMouseButtonUp(0) && zipper != null)
        {
            zipper = null;
            stopZipperSound = true;
        }
    }

    Ray GenerateMouseRay()
    {
        Vector3 mousePosFar = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.farClipPlane);
        Vector3 mousePosNear = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);

        Vector3 mousePosF = Camera.main.ScreenToWorldPoint(mousePosFar);
        Vector3 mousePosN = Camera.main.ScreenToWorldPoint(mousePosNear);
        Debug.DrawRay(mousePosN, mousePosF - mousePosN);
        return new Ray(mousePosN, mousePosF - mousePosN);
    }
}
