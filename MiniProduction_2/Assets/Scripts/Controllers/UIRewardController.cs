﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRewardController : Manager<UIRewardController>
{
    [SerializeField]
    public GameObject GoToBuildScene;

    [SerializeField]
    public GameObject StatPanel;

    [SerializeField]
    Text sleeveName;

    [SerializeField]
    Slider agilitySlider;
    [SerializeField]
    Slider attackSlider;
    [SerializeField]
    Slider healthSlider;
    [SerializeField]
    Slider defenseSlider;

    [SerializeField]
    GameObject GoToRightButton;
    [SerializeField]
    GameObject GoToLeftButton;

    [SerializeField]
    public GameObject ChoosePriceImage;


    // Use this for initialization
    void Start () {
		transform.parent = GameObject.Find("ParentForDestroy").transform;		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSliders(SleeveScriptableObject sleeve)
    {
        agilitySlider.value = sleeve.agility;
        attackSlider.value = sleeve.attack;
        healthSlider.value = sleeve.health;
        defenseSlider.value = sleeve.defense;
        sleeveName.text = sleeve.sleeveName;
    }

    public void SetSlidersEmpty()
    {
        agilitySlider.value = 0;
        attackSlider.value = 0;
        healthSlider.value = 0;
        defenseSlider.value = 0;
        sleeveName.text = "";
    }

    public void RotateRewards(string towards)
    {
        PlatformController.Instance().ResetBool();

        if (towards == "left")
        {
            PlatformController.Instance().MoveToLeft();
        }
        else if (towards == "right")
        {
            PlatformController.Instance().MoveToRight();
        }
    }

    public void ContinueToBuildScene()
    {
        SceneController.Instance().LoadBuildTeam();
    }
}
