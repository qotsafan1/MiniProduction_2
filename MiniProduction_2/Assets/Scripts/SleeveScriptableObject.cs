﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
[CreateAssetMenu(fileName = "new sleeve", menuName = "sleeve")]
public class SleeveScriptableObject : ScriptableObject {

    [Range(0, 100)]
    public int agility;
    [Range(0, 100)]
    public int attack;
    [Range(0, 1000)]
    public int health;

    public bool isMale;

    public int currentHealth; 
    [Range(0,100)]
    public int defense;
    public bool isActive;
    public bool isReady;

    string[] names = new string[] { "ROBO SAMURAI", "KILLER SHADOW", "ROBO UNICORN", "KILLER SPEEDY", "MASSIVE BEAST", "BULKY FOX", "BULKY ENVOY", 
    "SUPER ENVOY", "KILLER ENVOY", "MURDEROUS KNIGHT", "DUSTY KNIGHT", "MURDEROUS KNIGHT", "DUSTY SQUID", "ELITE SAMURAI", "BULKY GROUNDER", "FLUFFY ENVOY"};
    public string sleeveName = "Elias Ryker";

    public GameObject sleeveModelPrefab;
    public GameObject malePrefab;
    public GameObject femalePrefab;
    

    public void Randomize(int value)
    {

        isMale = (Random.Range(0, 2) != 1);
        if (isMale)
        {
            sleeveModelPrefab = malePrefab;
        }
        else
        {
            sleeveModelPrefab = femalePrefab;
        }

        float attackSplit = 0.1f;
        float agilitySplit = 0.05f;
        float defenseSplit = 0.05f;
        float statsVariance = 0.25f;


        float agilityVariance = 0.09533468f + (749673.1f - 0.09533468f) / (1 + (Mathf.Pow(value/ 0.000005659471f, 0.8891553f)));
        Debug.Log(agilityVariance);


        agility = (int)(value * agilitySplit);
        agility = (int)(agility + (agility * Random.Range(-agilityVariance, agilityVariance)));

        if (agility <= 0)
        {
            agility = agility + 1;
        }

        defense = (int)(value * defenseSplit);
        defense = (int)(defense + (defense * Random.Range(-statsVariance, statsVariance)));

        if (defense <= 0)
        {
            defense = defense + 1;
        }

        attack = (int)(value * attackSplit);
        attack = (int)(attack + (defense * Random.Range(-statsVariance, statsVariance)));

        if (attack <= 0)
        {
            attack = attack + 1;
        }

        health = value - agility - defense - attack;

        sleeveName = RandomName();
        Debug.Log(sleeveName);
        currentHealth = health;
    }


    public void RandomizeLegacy(int POWERLEVEL, int agilityMod, int attackMod, int healthMod, int defenseMod)
    {
        agility = Random.Range(0, 100);
        Vector4 vectorForNorm = new Vector4(Random.Range(0 + agilityMod, 100 + agilityMod), Random.Range(0 + attackMod, 100 + attackMod), Random.Range(0 + healthMod, 100 + healthMod), Random.Range(0 + defenseMod, 100 + defenseMod));
        float sum = vectorForNorm.x + vectorForNorm.y + vectorForNorm.z + vectorForNorm.w;
        vectorForNorm.x = vectorForNorm.x / sum;
        vectorForNorm.y = vectorForNorm.y / sum;
        vectorForNorm.z = vectorForNorm.z / sum;
        vectorForNorm.w = vectorForNorm.w / sum;

        vectorForNorm = vectorForNorm * POWERLEVEL;

        agility = (int)vectorForNorm.x;
        attack = (int)vectorForNorm.y;
        health = (int)vectorForNorm.z;
        defense = (int)vectorForNorm.w;
        sleeveName = names[Random.Range(0, names.Length)];
        currentHealth = health;
    }

    public void Randomizelegacy(int POWERLEVEL)
    {
        agility = Random.Range(0, 100);
        Vector4 vectorForNorm = new Vector4(Random.Range(0, 100), Random.Range(0, 100), Random.Range(0, 100), Random.Range(0, 100));
        float sum = vectorForNorm.x + vectorForNorm.y + vectorForNorm.z + vectorForNorm.w;
        vectorForNorm.x = vectorForNorm.x / sum;
        vectorForNorm.y = vectorForNorm.y / sum;
        vectorForNorm.z = vectorForNorm.z / sum;
        vectorForNorm.w = vectorForNorm.w / sum;

        vectorForNorm = vectorForNorm * POWERLEVEL;

        agility = (int)vectorForNorm.x;
        attack = (int)vectorForNorm.y;
        health = (int)vectorForNorm.z;
        defense = (int)vectorForNorm.w;
        sleeveName = names[Random.Range(0, names.Length)];
        currentHealth = health;
    }
    string RandomName()
    {
        return names[Random.Range(0,names.Length)];
    }
}
