﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

public class AnimationLoader : Manager<AnimationLoader> {
    public Dictionary<string, Dictionary<string, int[]>> Transitions = new Dictionary<string, Dictionary<string, int[]>>();
    public Dictionary<string, int> FrameCount = new Dictionary<string, int>();
    void Awake () {

        TextAsset transitionTextAsset = Resources.Load("Animation/transitions") as TextAsset;
        TextAsset frameCountAsset = Resources.Load("Animation/frames") as TextAsset;

        //Transitions = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int[]>>>(File.ReadAllText(@"Assets\Animations\transitions.json"));
        //FrameCount = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(@"Assets\Animations\frames.json"));
        Transitions = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int[]>>>(transitionTextAsset.text);
        FrameCount = JsonConvert.DeserializeObject<Dictionary<string, int>>(frameCountAsset.text);

        

	}

    public float FrameToTime(string animation, int frame)
    {
        return frame == 0 ? 0f : (float)frame / FrameCount[animation];
    }

    public int TimeToFrame(string animation, float time)
    {
        if (time > 1)
            return FrameCount[animation] - 1;
        else
            return time == 0f ? 0 : (int)Mathf.Round((FrameCount[animation]-1) * time);
    }
}
