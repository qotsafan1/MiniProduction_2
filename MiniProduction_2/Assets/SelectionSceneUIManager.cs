﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionSceneUIManager : Manager<SelectionSceneUIManager> {

   
    public Slider agilitySlider;
    public Slider attackSlider;
    public Slider healthSlider;
    public Slider defenseSlider;

    public GameObject gAgilitySlider;
    public GameObject gAttackSlider;
    public GameObject gHealthSlider;
    public GameObject gDefenseSlider;


    public GameObject gSelectFighter;
    public GameObject gDeSelectFighter;
    public GameObject gDestroySleeve;
    public GameObject gFight;
    public GameObject upcomingFighter;

    public Button DeSelectFighter;
    public Button SelectFighter;
    public Button goRight;
    public Button goLeft;
    public Button destroySleeve;
    public Button fight;
    public Text sleeveName;



    private void Start()
    {
        agilitySlider = gAgilitySlider.GetComponent<Slider>();
        attackSlider = gAttackSlider.GetComponent<Slider>();
        healthSlider = gHealthSlider.GetComponent<Slider>();
        defenseSlider = gDefenseSlider.GetComponent<Slider>();
        SelectFighter =gSelectFighter.GetComponent<Button>();
        destroySleeve = gDestroySleeve.GetComponent<Button>();
        DeSelectFighter = gDeSelectFighter.GetComponent<Button>();


    }

}
